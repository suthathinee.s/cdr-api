require "test_helper"

class PipeItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pipe_item = pipe_items(:one)
  end

  test "should get index" do
    get pipe_items_url, as: :json
    assert_response :success
  end

  test "should create pipe_item" do
    assert_difference("PipeItem.count") do
      post pipe_items_url, params: { pipe_item: { col_position: @pipe_item.col_position, element_id_id: @pipe_item.element_id_id, pipe_id_id: @pipe_item.pipe_id_id, rotation: @pipe_item.rotation, row_position: @pipe_item.row_position } }, as: :json
    end

    assert_response :created
  end

  test "should show pipe_item" do
    get pipe_item_url(@pipe_item), as: :json
    assert_response :success
  end

  test "should update pipe_item" do
    patch pipe_item_url(@pipe_item), params: { pipe_item: { col_position: @pipe_item.col_position, element_id_id: @pipe_item.element_id_id, pipe_id_id: @pipe_item.pipe_id_id, rotation: @pipe_item.rotation, row_position: @pipe_item.row_position } }, as: :json
    assert_response :success
  end

  test "should destroy pipe_item" do
    assert_difference("PipeItem.count", -1) do
      delete pipe_item_url(@pipe_item), as: :json
    end

    assert_response :no_content
  end
end
