require "test_helper"

class WorkingMemorySizesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @working_memory_size = working_memory_sizes(:one)
  end

  test "should get index" do
    get working_memory_sizes_url, as: :json
    assert_response :success
  end

  test "should create working_memory_size" do
    assert_difference("WorkingMemorySize.count") do
      post working_memory_sizes_url, params: { working_memory_size: { name: @working_memory_size.name } }, as: :json
    end

    assert_response :created
  end

  test "should show working_memory_size" do
    get working_memory_size_url(@working_memory_size), as: :json
    assert_response :success
  end

  test "should update working_memory_size" do
    patch working_memory_size_url(@working_memory_size), params: { working_memory_size: { name: @working_memory_size.name } }, as: :json
    assert_response :success
  end

  test "should destroy working_memory_size" do
    assert_difference("WorkingMemorySize.count", -1) do
      delete working_memory_size_url(@working_memory_size), as: :json
    end

    assert_response :no_content
  end
end
