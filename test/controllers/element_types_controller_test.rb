require "test_helper"

class ElementTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @element_type = element_types(:one)
  end

  test "should get index" do
    get element_types_url, as: :json
    assert_response :success
  end

  test "should create element_type" do
    assert_difference("ElementType.count") do
      post element_types_url, params: { element_type: { name: @element_type.name } }, as: :json
    end

    assert_response :created
  end

  test "should show element_type" do
    get element_type_url(@element_type), as: :json
    assert_response :success
  end

  test "should update element_type" do
    patch element_type_url(@element_type), params: { element_type: { name: @element_type.name } }, as: :json
    assert_response :success
  end

  test "should destroy element_type" do
    assert_difference("ElementType.count", -1) do
      delete element_type_url(@element_type), as: :json
    end

    assert_response :no_content
  end
end
