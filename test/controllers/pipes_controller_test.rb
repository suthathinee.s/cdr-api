require "test_helper"

class PipesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pipe = pipes(:one)
  end

  test "should get index" do
    get pipes_url, as: :json
    assert_response :success
  end

  test "should create pipe" do
    assert_difference("Pipe.count") do
      post pipes_url, params: { pipe: { max_times: @pipe.max_times, max_click: @pipe.max_click, name: @pipe.name, pipe_dimension_id_id: @pipe.pipe_dimension_id_id, status_id_id: @pipe.status_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show pipe" do
    get pipe_url(@pipe), as: :json
    assert_response :success
  end

  test "should update pipe" do
    patch pipe_url(@pipe), params: { pipe: { max_times: @pipe.max_times, max_click: @pipe.max_click, name: @pipe.name, pipe_dimension_id_id: @pipe.pipe_dimension_id_id, status_id_id: @pipe.status_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy pipe" do
    assert_difference("Pipe.count", -1) do
      delete pipe_url(@pipe), as: :json
    end

    assert_response :no_content
  end
end
