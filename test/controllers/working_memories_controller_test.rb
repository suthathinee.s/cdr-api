require "test_helper"

class WorkingMemoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @working_memory = working_memories(:one)
  end

  test "should get index" do
    get working_memories_url, as: :json
    assert_response :success
  end

  test "should create working_memory" do
    assert_difference("WorkingMemory.count") do
      post working_memories_url, params: { working_memory: { max_times: @working_memory.max_times, name: @working_memory.name, status_id_id: @working_memory.status_id_id, working_memory_size_id_id: @working_memory.working_memory_size_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show working_memory" do
    get working_memory_url(@working_memory), as: :json
    assert_response :success
  end

  test "should update working_memory" do
    patch working_memory_url(@working_memory), params: { working_memory: { max_times: @working_memory.max_times, name: @working_memory.name, status_id_id: @working_memory.status_id_id, working_memory_size_id_id: @working_memory.working_memory_size_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy working_memory" do
    assert_difference("WorkingMemory.count", -1) do
      delete working_memory_url(@working_memory), as: :json
    end

    assert_response :no_content
  end
end
