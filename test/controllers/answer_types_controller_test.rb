require "test_helper"

class AnswerTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @answer_type = answer_types(:one)
  end

  test "should get index" do
    get answer_types_url, as: :json
    assert_response :success
  end

  test "should create answer_type" do
    assert_difference("AnswerType.count") do
      post answer_types_url, params: { answer_type: { name: @answer_type.name } }, as: :json
    end

    assert_response :created
  end

  test "should show answer_type" do
    get answer_type_url(@answer_type), as: :json
    assert_response :success
  end

  test "should update answer_type" do
    patch answer_type_url(@answer_type), params: { answer_type: { name: @answer_type.name } }, as: :json
    assert_response :success
  end

  test "should destroy answer_type" do
    assert_difference("AnswerType.count", -1) do
      delete answer_type_url(@answer_type), as: :json
    end

    assert_response :no_content
  end
end
