require "test_helper"

class PipeDimensionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pipe_dimension = pipe_dimensions(:one)
  end

  test "should get index" do
    get pipe_dimensions_url, as: :json
    assert_response :success
  end

  test "should create pipe_dimension" do
    assert_difference("PipeDimension.count") do
      post pipe_dimensions_url, params: { pipe_dimension: { col: @pipe_dimension.col, name: @pipe_dimension.name, row: @pipe_dimension.row } }, as: :json
    end

    assert_response :created
  end

  test "should show pipe_dimension" do
    get pipe_dimension_url(@pipe_dimension), as: :json
    assert_response :success
  end

  test "should update pipe_dimension" do
    patch pipe_dimension_url(@pipe_dimension), params: { pipe_dimension: { col: @pipe_dimension.col, name: @pipe_dimension.name, row: @pipe_dimension.row } }, as: :json
    assert_response :success
  end

  test "should destroy pipe_dimension" do
    assert_difference("PipeDimension.count", -1) do
      delete pipe_dimension_url(@pipe_dimension), as: :json
    end

    assert_response :no_content
  end
end
