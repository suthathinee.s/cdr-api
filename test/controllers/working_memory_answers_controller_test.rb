require "test_helper"

class WorkingMemoryAnswersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @working_memory_answer = working_memory_answers(:one)
  end

  test "should get index" do
    get working_memory_answers_url, as: :json
    assert_response :success
  end

  test "should create working_memory_answer" do
    assert_difference("WorkingMemoryAnswer.count") do
      post working_memory_answers_url, params: { working_memory_answer: { answer_correct: @working_memory_answer.answer_correct, answer_wrong: @working_memory_answer.answer_wrong, assignment_id_id: @working_memory_answer.assignment_id_id, assignment_item_id_id: @working_memory_answer.assignment_item_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show working_memory_answer" do
    get working_memory_answer_url(@working_memory_answer), as: :json
    assert_response :success
  end

  test "should update working_memory_answer" do
    patch working_memory_answer_url(@working_memory_answer), params: { working_memory_answer: { answer_correct: @working_memory_answer.answer_correct, answer_wrong: @working_memory_answer.answer_wrong, assignment_id_id: @working_memory_answer.assignment_id_id, assignment_item_id_id: @working_memory_answer.assignment_item_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy working_memory_answer" do
    assert_difference("WorkingMemoryAnswer.count", -1) do
      delete working_memory_answer_url(@working_memory_answer), as: :json
    end

    assert_response :no_content
  end
end
