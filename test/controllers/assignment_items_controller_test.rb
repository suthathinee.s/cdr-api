require "test_helper"

class AssignmentItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assignment_item = assignment_items(:one)
  end

  test "should get index" do
    get assignment_items_url, as: :json
    assert_response :success
  end

  test "should create assignment_item" do
    assert_difference("AssignmentItem.count") do
      post assignment_items_url, params: { assignment_item: { assingnment_id_id: @assignment_item.assingnment_id_id, game_type_id_id: @assignment_item.game_type_id_id, order_no: @assignment_item.order_no, pipe_serie_id_id: @assignment_item.pipe_serie_id_id, working_memory_serie_id_id: @assignment_item.working_memory_serie_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show assignment_item" do
    get assignment_item_url(@assignment_item), as: :json
    assert_response :success
  end

  test "should update assignment_item" do
    patch assignment_item_url(@assignment_item), params: { assignment_item: { assingnment_id_id: @assignment_item.assingnment_id_id, game_type_id_id: @assignment_item.game_type_id_id, order_no: @assignment_item.order_no, pipe_serie_id_id: @assignment_item.pipe_serie_id_id, working_memory_serie_id_id: @assignment_item.working_memory_serie_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy assignment_item" do
    assert_difference("AssignmentItem.count", -1) do
      delete assignment_item_url(@assignment_item), as: :json
    end

    assert_response :no_content
  end
end
