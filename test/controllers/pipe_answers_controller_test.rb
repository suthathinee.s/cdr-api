require "test_helper"

class PipeAnswersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pipe_answer = pipe_answers(:one)
  end

  test "should get index" do
    get pipe_answers_url, as: :json
    assert_response :success
  end

  test "should create pipe_answer" do
    assert_difference("PipeAnswer.count") do
      post pipe_answers_url, params: { pipe_answer: { assignment_id_id: @pipe_answer.assignment_id_id, assignment_item_id_id: @pipe_answer.assignment_item_id_id, click_amount: @pipe_answer.click_amount, limit_time: @pipe_answer.limit_time, spent_time: @pipe_answer.spent_time } }, as: :json
    end

    assert_response :created
  end

  test "should show pipe_answer" do
    get pipe_answer_url(@pipe_answer), as: :json
    assert_response :success
  end

  test "should update pipe_answer" do
    patch pipe_answer_url(@pipe_answer), params: { pipe_answer: { assignment_id_id: @pipe_answer.assignment_id_id, assignment_item_id_id: @pipe_answer.assignment_item_id_id, click_amount: @pipe_answer.click_amount, limit_time: @pipe_answer.limit_time, spent_time: @pipe_answer.spent_time } }, as: :json
    assert_response :success
  end

  test "should destroy pipe_answer" do
    assert_difference("PipeAnswer.count", -1) do
      delete pipe_answer_url(@pipe_answer), as: :json
    end

    assert_response :no_content
  end
end
