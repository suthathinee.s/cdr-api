require "test_helper"

class WorkingMemorySeriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @working_memory_series = working_memory_series(:one)
  end

  test "should get index" do
    get working_memory_series_index_url, as: :json
    assert_response :success
  end

  test "should create working_memory_series" do
    assert_difference("WorkingMemorySeries.count") do
      post working_memory_series_index_url, params: { working_memory_series: { name: @working_memory_series.name, status_id_id: @working_memory_series.status_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show working_memory_series" do
    get working_memory_series_url(@working_memory_series), as: :json
    assert_response :success
  end

  test "should update working_memory_series" do
    patch working_memory_series_url(@working_memory_series), params: { working_memory_series: { name: @working_memory_series.name, status_id_id: @working_memory_series.status_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy working_memory_series" do
    assert_difference("WorkingMemorySeries.count", -1) do
      delete working_memory_series_url(@working_memory_series), as: :json
    end

    assert_response :no_content
  end
end
