require "test_helper"

class PipeSerieStagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pipe_serie_stage = pipe_serie_stages(:one)
  end

  test "should get index" do
    get pipe_serie_stages_url, as: :json
    assert_response :success
  end

  test "should create pipe_serie_stage" do
    assert_difference("PipeSerieStage.count") do
      post pipe_serie_stages_url, params: { pipe_serie_stage: { order_no: @pipe_serie_stage.order_no, pipe_id_id: @pipe_serie_stage.pipe_id_id, pipe_serie_id_id: @pipe_serie_stage.pipe_serie_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show pipe_serie_stage" do
    get pipe_serie_stage_url(@pipe_serie_stage), as: :json
    assert_response :success
  end

  test "should update pipe_serie_stage" do
    patch pipe_serie_stage_url(@pipe_serie_stage), params: { pipe_serie_stage: { order_no: @pipe_serie_stage.order_no, pipe_id_id: @pipe_serie_stage.pipe_id_id, pipe_serie_id_id: @pipe_serie_stage.pipe_serie_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy pipe_serie_stage" do
    assert_difference("PipeSerieStage.count", -1) do
      delete pipe_serie_stage_url(@pipe_serie_stage), as: :json
    end

    assert_response :no_content
  end
end
