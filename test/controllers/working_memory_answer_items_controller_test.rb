require "test_helper"

class WorkingMemoryAnswerItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @working_memory_answer_item = working_memory_answer_items(:one)
  end

  test "should get index" do
    get working_memory_answer_items_url, as: :json
    assert_response :success
  end

  test "should create working_memory_answer_item" do
    assert_difference("WorkingMemoryAnswerItem.count") do
      post working_memory_answer_items_url, params: { working_memory_answer_item: { answer: @working_memory_answer_item.answer, answer_correct: @working_memory_answer_item.answer_correct, answer_wrong: @working_memory_answer_item.answer_wrong, working_memory_answer_id_id: @working_memory_answer_item.working_memory_answer_id_id, working_memory_id_id: @working_memory_answer_item.working_memory_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show working_memory_answer_item" do
    get working_memory_answer_item_url(@working_memory_answer_item), as: :json
    assert_response :success
  end

  test "should update working_memory_answer_item" do
    patch working_memory_answer_item_url(@working_memory_answer_item), params: { working_memory_answer_item: { answer: @working_memory_answer_item.answer, answer_correct: @working_memory_answer_item.answer_correct, answer_wrong: @working_memory_answer_item.answer_wrong, working_memory_answer_id_id: @working_memory_answer_item.working_memory_answer_id_id, working_memory_id_id: @working_memory_answer_item.working_memory_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy working_memory_answer_item" do
    assert_difference("WorkingMemoryAnswerItem.count", -1) do
      delete working_memory_answer_item_url(@working_memory_answer_item), as: :json
    end

    assert_response :no_content
  end
end
