require "test_helper"

class PipeAnswerItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pipe_answer_item = pipe_answer_items(:one)
  end

  test "should get index" do
    get pipe_answer_items_url, as: :json
    assert_response :success
  end

  test "should create pipe_answer_item" do
    assert_difference("PipeAnswerItem.count") do
      post pipe_answer_items_url, params: { pipe_answer_item: { answer_obj: @pipe_answer_item.answer_obj, answer_type_id_id: @pipe_answer_item.answer_type_id_id, click_amount: @pipe_answer_item.click_amount, limit_time: @pipe_answer_item.limit_time, pipe_answer_id_id: @pipe_answer_item.pipe_answer_id_id, pipe_id_id: @pipe_answer_item.pipe_id_id, spent_time: @pipe_answer_item.spent_time } }, as: :json
    end

    assert_response :created
  end

  test "should show pipe_answer_item" do
    get pipe_answer_item_url(@pipe_answer_item), as: :json
    assert_response :success
  end

  test "should update pipe_answer_item" do
    patch pipe_answer_item_url(@pipe_answer_item), params: { pipe_answer_item: { answer_obj: @pipe_answer_item.answer_obj, answer_type_id_id: @pipe_answer_item.answer_type_id_id, click_amount: @pipe_answer_item.click_amount, limit_time: @pipe_answer_item.limit_time, pipe_answer_id_id: @pipe_answer_item.pipe_answer_id_id, pipe_id_id: @pipe_answer_item.pipe_id_id, spent_time: @pipe_answer_item.spent_time } }, as: :json
    assert_response :success
  end

  test "should destroy pipe_answer_item" do
    assert_difference("PipeAnswerItem.count", -1) do
      delete pipe_answer_item_url(@pipe_answer_item), as: :json
    end

    assert_response :no_content
  end
end
