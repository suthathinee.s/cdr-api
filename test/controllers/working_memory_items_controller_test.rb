require "test_helper"

class WorkingMemoryItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @working_memory_item = working_memory_items(:one)
  end

  test "should get index" do
    get working_memory_items_url, as: :json
    assert_response :success
  end

  test "should create working_memory_item" do
    assert_difference("WorkingMemoryItem.count") do
      post working_memory_items_url, params: { working_memory_item: { element_id_id: @working_memory_item.element_id_id, position: @working_memory_item.position, working_memory_id_id: @working_memory_item.working_memory_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show working_memory_item" do
    get working_memory_item_url(@working_memory_item), as: :json
    assert_response :success
  end

  test "should update working_memory_item" do
    patch working_memory_item_url(@working_memory_item), params: { working_memory_item: { element_id_id: @working_memory_item.element_id_id, position: @working_memory_item.position, working_memory_id_id: @working_memory_item.working_memory_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy working_memory_item" do
    assert_difference("WorkingMemoryItem.count", -1) do
      delete working_memory_item_url(@working_memory_item), as: :json
    end

    assert_response :no_content
  end
end
