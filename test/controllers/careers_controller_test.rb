require "test_helper"

class CareersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @career = careers(:one)
  end

  test "should get index" do
    get careers_url, as: :json
    assert_response :success
  end

  test "should create career" do
    assert_difference("Career.count") do
      post careers_url, params: { career: { name: @career.name } }, as: :json
    end

    assert_response :created
  end

  test "should show career" do
    get career_url(@career), as: :json
    assert_response :success
  end

  test "should update career" do
    patch career_url(@career), params: { career: { name: @career.name } }, as: :json
    assert_response :success
  end

  test "should destroy career" do
    assert_difference("Career.count", -1) do
      delete career_url(@career), as: :json
    end

    assert_response :no_content
  end
end
