require "test_helper"

class PipeSeriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pipe_series = pipe_series(:one)
  end

  test "should get index" do
    get pipe_series_index_url, as: :json
    assert_response :success
  end

  test "should create pipe_series" do
    assert_difference("PipeSeries.count") do
      post pipe_series_index_url, params: { pipe_series: { name: @pipe_series.name, status_id_id: @pipe_series.status_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show pipe_series" do
    get pipe_series_url(@pipe_series), as: :json
    assert_response :success
  end

  test "should update pipe_series" do
    patch pipe_series_url(@pipe_series), params: { pipe_series: { name: @pipe_series.name, status_id_id: @pipe_series.status_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy pipe_series" do
    assert_difference("PipeSeries.count", -1) do
      delete pipe_series_url(@pipe_series), as: :json
    end

    assert_response :no_content
  end
end
