require "test_helper"

class WorkingMemorySerieStagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @working_memory_serie_stage = working_memory_serie_stages(:one)
  end

  test "should get index" do
    get working_memory_serie_stages_url, as: :json
    assert_response :success
  end

  test "should create working_memory_serie_stage" do
    assert_difference("WorkingMemorySerieStage.count") do
      post working_memory_serie_stages_url, params: { working_memory_serie_stage: { order_no: @working_memory_serie_stage.order_no, working_memory_id_id: @working_memory_serie_stage.working_memory_id_id, working_memory_serie_id_id: @working_memory_serie_stage.working_memory_serie_id_id } }, as: :json
    end

    assert_response :created
  end

  test "should show working_memory_serie_stage" do
    get working_memory_serie_stage_url(@working_memory_serie_stage), as: :json
    assert_response :success
  end

  test "should update working_memory_serie_stage" do
    patch working_memory_serie_stage_url(@working_memory_serie_stage), params: { working_memory_serie_stage: { order_no: @working_memory_serie_stage.order_no, working_memory_id_id: @working_memory_serie_stage.working_memory_id_id, working_memory_serie_id_id: @working_memory_serie_stage.working_memory_serie_id_id } }, as: :json
    assert_response :success
  end

  test "should destroy working_memory_serie_stage" do
    assert_difference("WorkingMemorySerieStage.count", -1) do
      delete working_memory_serie_stage_url(@working_memory_serie_stage), as: :json
    end

    assert_response :no_content
  end
end
