require "test_helper"

class RolePermissionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @role_permission = role_permissions(:one)
  end

  test "should get index" do
    get role_permissions_url, as: :json
    assert_response :success
  end

  test "should create role_permission" do
    assert_difference("RolePermission.count") do
      post role_permissions_url, params: { role_permission: { name: @role_permission.name } }, as: :json
    end

    assert_response :created
  end

  test "should show role_permission" do
    get role_permission_url(@role_permission), as: :json
    assert_response :success
  end

  test "should update role_permission" do
    patch role_permission_url(@role_permission), params: { role_permission: { name: @role_permission.name } }, as: :json
    assert_response :success
  end

  test "should destroy role_permission" do
    assert_difference("RolePermission.count", -1) do
      delete role_permission_url(@role_permission), as: :json
    end

    assert_response :no_content
  end
end
