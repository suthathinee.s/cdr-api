class AddTimeColumnsToAssignments < ActiveRecord::Migration[7.0]
  def change
    add_column :assignments, :timestart, :datetime
    add_column :assignments, :timestop, :datetime
  end
end
