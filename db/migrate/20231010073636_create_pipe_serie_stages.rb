class CreatePipeSerieStages < ActiveRecord::Migration[7.0]
  def change
    create_table :pipe_serie_stages do |t|
      t.references :pipe_series, foreign_key: {on_delete: :cascade}
      t.references :pipe, foreign_key: {on_delete: :cascade}
      t.integer :order_no

      t.timestamps
    end
  end
end
