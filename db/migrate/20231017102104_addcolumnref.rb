class Addcolumnref < ActiveRecord::Migration[7.0]
  def change
    add_reference :patients, :career, foreign_key: true
    add_reference :patients, :gender, foreign_key: true
    add_reference :doctors, :role_permission, foreign_key: {on_delete: :cascade}
    add_reference :users, :doctor, foreign_key: {on_delete: :cascade}
  end
  Rake::Task["migration:load_elements"].invoke
  Rake::Task["migration:load_users"].invoke
end
