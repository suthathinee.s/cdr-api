class CreatePipeAnswers < ActiveRecord::Migration[7.0]
  def change
    create_table :pipe_answers do |t|
      t.references :assignment, foreign_key: {on_delete: :cascade}
      t.references :assignment_item, foreign_key: {on_delete: :cascade}
      t.integer :click_amount
      t.integer :spent_time
      t.integer :limit_time

      t.timestamps
    end
  end
end
