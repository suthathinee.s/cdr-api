class AddGameTypeToPipeSeries < ActiveRecord::Migration[7.0]
  def change
    add_reference :pipe_series, :game_type, foreign_key:[on_delete: :restrict]
  end
end
