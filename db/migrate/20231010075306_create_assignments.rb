class CreateAssignments < ActiveRecord::Migration[7.0]
  def change
    create_table :assignments do |t|
      t.references :patient, foreign_key: {on_delete: :cascade}
      t.references :doctor, foreign_key: {on_delete: :cascade}
      t.string :code, index: true

      t.timestamps
    end
  end
end
