class CreateGameTypes < ActiveRecord::Migration[7.0]
  def change
    create_table :game_types do |t|
      t.string :name

      t.timestamps
    end
    Rake::Task["migration:load_game_types"].invoke
  end
end
