class AddStartDateAndEndDateToPipeAnswers < ActiveRecord::Migration[7.0]
  def change
    add_column :pipe_answers, :start_date, :datetime
    add_column :pipe_answers, :end_date, :datetime
  end
end
