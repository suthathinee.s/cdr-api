class CreateAnswerTypes < ActiveRecord::Migration[7.0]
  def change
    create_table :answer_types do |t|
      t.string :name

      t.timestamps
    end
    Rake::Task["migration:load_answer_type"].invoke
  end
end
