class CreatePipeDimensions < ActiveRecord::Migration[7.0]
  def change
    create_table :pipe_dimensions do |t|
      t.integer :row
      t.integer :col
      t.string :name

      t.timestamps
    end
    Rake::Task["migration:load_pipe_dimension"].invoke
  end
end
