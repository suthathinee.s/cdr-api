class CreatePipeAnswerItems < ActiveRecord::Migration[7.0]
  def change
    create_table :pipe_answer_items do |t|
      t.references :pipe_answer, foreign_key: {on_delete: :cascade}
      t.jsonb :answer_obj
      t.references :pipe, foreign_key: {on_delete: :cascade}
      t.integer :click_amount
      t.integer :spent_time
      t.integer :limit_time
      t.references :answer_type,  foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
