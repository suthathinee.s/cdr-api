class CreateWorkingMemorySizes < ActiveRecord::Migration[7.0]
  def change
    create_table :working_memory_sizes do |t|
      t.string :name
      t.integer :size

      t.timestamps
    end
    Rake::Task["migration:load_working_memory_sizes"].invoke
  end
end
