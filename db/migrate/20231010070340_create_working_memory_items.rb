class CreateWorkingMemoryItems < ActiveRecord::Migration[7.0]
  def change
    create_table :working_memory_items do |t|
      t.references :working_memory, foreign_key: {on_delete: :cascade}
      t.references :element, foreign_key: {on_delete: :cascade}
      t.integer :position

      t.timestamps
    end
  end
end
