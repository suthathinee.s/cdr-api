class CreateWorkingMemories < ActiveRecord::Migration[7.0]
  def change
    create_table :working_memories do |t|
      t.string :name
      t.integer :max_times
      t.references :working_memory_size, foreign_key: {on_delete: :cascade}
      t.references :status, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
