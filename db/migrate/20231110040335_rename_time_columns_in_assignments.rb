class RenameTimeColumnsInAssignments < ActiveRecord::Migration[7.0]
  def change
    rename_column :assignments, :timestart, :start_date
    rename_column :assignments, :timestop, :end_date
  end
end
