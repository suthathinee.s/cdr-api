class AddUsernameToLogins < ActiveRecord::Migration[7.0]
  def change
    add_column :logins, :username, :string
  end
end
