class AddReferencesToAssignmentItemsAndPatients < ActiveRecord::Migration[7.0]
  def change
    add_reference :assignment_items, :assignment, foreign_key: {on_delete: :cascade}
    add_reference :assignment_items, :game_type, foreign_key: {on_delete: :cascade}
    add_reference :assignment_items, :working_memory_serie, foreign_key: {on_delete: :cascade}
    add_reference :assignment_items, :pipe_serie, foreign_key: {on_delete: :cascade}
    add_reference :patients, :career, foreign_key: {on_delete: :cascade}
    add_reference :patients, :gender, foreign_key: {on_delete: :cascade}
  end
end
