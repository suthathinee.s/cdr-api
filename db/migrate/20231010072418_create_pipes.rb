class CreatePipes < ActiveRecord::Migration[7.0]
  def change
    create_table :pipes do |t|
      t.references :pipe_dimension, foreign_key: {on_delete: :cascade}
      t.integer :max_click
      t.integer :max_times
      t.string :name
      t.references :status, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
