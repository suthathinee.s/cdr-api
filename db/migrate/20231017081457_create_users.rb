class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.references :role_permission, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
