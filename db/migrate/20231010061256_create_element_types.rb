class CreateElementTypes < ActiveRecord::Migration[7.0]
  def change
    create_table :element_types do |t|
      t.string :name

      t.timestamps
    end
    Rake::Task["migration:load_element_types"].invoke
  end
end
