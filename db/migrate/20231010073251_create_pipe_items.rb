class CreatePipeItems < ActiveRecord::Migration[7.0]
  def change
    create_table :pipe_items do |t|
      t.references :pipe, foreign_key: {on_delete: :cascade}
      t.references :element, foreign_key: {on_delete: :cascade}
      t.integer :rotation
      t.integer :row_position
      t.integer :col_position

      t.timestamps
    end
  end
end
