class CreateCareers < ActiveRecord::Migration[7.0]
  def change
    create_table :careers do |t|
      t.string :name

      t.timestamps
    end
    Rake::Task["migration:load_careers"].invoke
  end
end
