class CreateWorkingMemorySerieStages < ActiveRecord::Migration[7.0]
  def change
    create_table :working_memory_serie_stages do |t|
      t.references :working_memory_series, foreign_key: {on_delete: :cascade}
      t.references :working_memory, foreign_key: {on_delete: :cascade}
      t.integer :order_no

      t.timestamps
    end
  end
end
