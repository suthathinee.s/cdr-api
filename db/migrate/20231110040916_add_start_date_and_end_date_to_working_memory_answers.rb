class AddStartDateAndEndDateToWorkingMemoryAnswers < ActiveRecord::Migration[7.0]
  def change
    add_column :working_memory_answers, :start_date, :datetime
    add_column :working_memory_answers, :end_date, :datetime
  end
end
