class ChangeForeignKeyOnDeleteToRestrict < ActiveRecord::Migration[7.0]
  def change
    # Change on_delete to :restrict for elements table
    remove_foreign_key :elements, :element_types
    remove_foreign_key :elements, :game_types
    add_foreign_key :elements, :element_types, on_delete: :restrict
    add_foreign_key :elements, :game_types, on_delete: :restrict

    # Change on_delete to :restrict for working_memory_series table
    remove_foreign_key :working_memory_series, :statuses
    add_foreign_key :working_memory_series, :statuses, on_delete: :restrict

    # Change on_delete to :restrict for working_memories table
    remove_foreign_key :working_memories, :working_memory_sizes
    remove_foreign_key :working_memories, :statuses
    add_foreign_key :working_memories, :working_memory_sizes, on_delete: :restrict
    add_foreign_key :working_memories, :statuses, on_delete: :restrict

    # Change on_delete to :restrict for working_memory_items table
    remove_foreign_key :working_memory_items, :working_memories
    remove_foreign_key :working_memory_items, :elements
    add_foreign_key :working_memory_items, :working_memories, on_delete: :restrict
    add_foreign_key :working_memory_items, :elements, on_delete: :restrict

    # Change on_delete to :restrict for working_memory_serie_stages table
    remove_foreign_key :working_memory_serie_stages, :working_memory_series
    remove_foreign_key :working_memory_serie_stages, :working_memories
    add_foreign_key :working_memory_serie_stages, :working_memory_series, on_delete: :restrict
    add_foreign_key :working_memory_serie_stages, :working_memories, on_delete: :restrict

    # Change on_delete to :restrict for pipe_series table
    remove_foreign_key :pipe_series, :statuses
    add_foreign_key :pipe_series, :statuses, on_delete: :restrict

    # Change on_delete to :restrict for pipes table
    remove_foreign_key :pipes, :pipe_dimensions
    remove_foreign_key :pipes, :statuses
    add_foreign_key :pipes, :pipe_dimensions, on_delete: :restrict
    add_foreign_key :pipes, :statuses, on_delete: :restrict

    # Change on_delete to :restrict for pipe_items table
    remove_foreign_key :pipe_items, :pipes
    remove_foreign_key :pipe_items, :elements
    add_foreign_key :pipe_items, :pipes, on_delete: :restrict
    add_foreign_key :pipe_items, :elements, on_delete: :restrict

    # Change on_delete to :restrict for pipe_serie_stages table
    remove_foreign_key :pipe_serie_stages, :pipe_series
    remove_foreign_key :pipe_serie_stages, :pipes
    add_foreign_key :pipe_serie_stages, :pipe_series, on_delete: :restrict
    add_foreign_key :pipe_serie_stages, :pipes, on_delete: :restrict

    # Change on_delete to :restrict for assignments table
    remove_foreign_key :assignments, :patients
    remove_foreign_key :assignments, :doctors
    add_foreign_key :assignments, :patients, on_delete: :restrict
    add_foreign_key :assignments, :doctors, on_delete: :restrict

    # Change on_delete to :restrict for users table
    remove_foreign_key :users, :role_permissions
    remove_foreign_key :users, :doctors
    add_foreign_key :users, :role_permissions, on_delete: :restrict
    add_foreign_key :users, :doctors, on_delete: :restrict

    # Change on_delete to :restrict for patients table
    remove_foreign_key :patients, :careers
    remove_foreign_key :patients, :genders
    add_foreign_key :patients, :careers, on_delete: :restrict
    add_foreign_key :patients, :genders, on_delete: :restrict

    # Change on_delete to :restrict for doctors table
    remove_foreign_key :doctors, :role_permissions
    remove_foreign_key :doctors, :genders
    add_foreign_key :doctors, :role_permissions, on_delete: :restrict
    add_foreign_key :doctors, :genders, on_delete: :restrict

    # Change on_delete to :restrict for pipe_answer_items table
    remove_foreign_key :pipe_answer_items, :answer_types
    add_foreign_key :pipe_answer_items, :answer_types, on_delete: :restrict

    # Change on_delete to :restrict for assignment_items table
    remove_foreign_key :assignment_items, :assignments
    remove_foreign_key :assignment_items, :game_types
    remove_foreign_key :assignment_items, :working_memory_series
    remove_foreign_key :assignment_items, :pipe_series
    add_foreign_key :assignment_items, :assignments, column: :assignment_id, on_delete: :restrict
    add_foreign_key :assignment_items, :game_types, column: :game_type_id, on_delete: :restrict
    add_foreign_key :assignment_items, :working_memory_series, column: :working_memory_serie_id, on_delete: :restrict
    add_foreign_key :assignment_items, :pipe_series, column: :pipe_serie_id, on_delete: :restrict
  end
end