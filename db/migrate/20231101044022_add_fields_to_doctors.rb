class AddFieldsToDoctors < ActiveRecord::Migration[7.0]
  def change
    add_column :doctors, :birthday, :date
    add_column :doctors, :years_of_education, :string
    add_reference :doctors, :gender, foreign_key: {on_delete: :cascade}
  end
end
