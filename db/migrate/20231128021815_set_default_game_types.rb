# db/migrate/20231128120000_set_default_game_types.rb (use the appropriate timestamp)
class SetDefaultGameTypes < ActiveRecord::Migration[7.0]
  def change
    change_column_default :working_memory_series, :game_type_id, 1
    change_column_default :pipe_series, :game_type_id, 2
  end
end
