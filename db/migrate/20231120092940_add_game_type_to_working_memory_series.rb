class AddGameTypeToWorkingMemorySeries < ActiveRecord::Migration[7.0]
  def change
    add_reference :working_memory_series, :game_type, foreign_key:[on_delete: :restrict]
  end
end
