class CreateRolePermissions < ActiveRecord::Migration[7.0]
  def change
    create_table :role_permissions do |t|
      t.string :name

      t.timestamps
    end
    Rake::Task["migration:load_role_permissions"].invoke
  end
end
