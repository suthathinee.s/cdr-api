class CreateWorkingMemorySeries < ActiveRecord::Migration[7.0]
  def change
    create_table :working_memory_series do |t|
      t.string :name
      t.references :status, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
