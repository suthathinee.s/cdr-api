class CreateAssignmentItems < ActiveRecord::Migration[7.0]
  def change
    create_table :assignment_items do |t|
      t.references :assignment, foreign_key: true
      t.integer :order_no
      t.references :game_type, foreign_key: true
      t.references :working_memory_serie, foreign_key: true
      t.references :pipe_serie, foreign_key: true

      t.timestamps
    end
  end
end
