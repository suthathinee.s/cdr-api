class CreateElements < ActiveRecord::Migration[7.0]
  def change
    create_table :elements do |t|
      t.string :file_url
      t.references :element_type, foreign_key: {on_delete: :cascade}
      t.references :game_type, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
