class AddNameToAssignments < ActiveRecord::Migration[7.0]
  def change
    add_column :assignments, :name, :string
  end
end
