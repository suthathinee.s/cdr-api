class RemoveReferencesFromAssignmentItems < ActiveRecord::Migration[7.0]
  def change
    remove_reference :assignment_items, :assignment, foreign_key: true
    remove_reference :assignment_items, :game_type, foreign_key: true
    remove_reference :assignment_items, :working_memory_serie, foreign_key: true
    remove_reference :assignment_items, :pipe_serie, foreign_key: true

    remove_reference :patients, :career, foreign_key: true
    remove_reference :patients, :gender, foreign_key: true
  end
end
