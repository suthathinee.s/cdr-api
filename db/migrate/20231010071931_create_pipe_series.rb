class CreatePipeSeries < ActiveRecord::Migration[7.0]
  def change
    create_table :pipe_series do |t|
      t.string :name
      t.references :status, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
