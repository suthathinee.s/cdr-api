class CreateWorkingMemoryAnswers < ActiveRecord::Migration[7.0]
  def change
    create_table :working_memory_answers do |t|
      t.references :assignment, foreign_key: {on_delete: :cascade}
      t.references :assignment_item, foreign_key: {on_delete: :cascade}
      t.integer :answer_correct
      t.integer :answer_wrong

      t.timestamps
    end
  end
end
