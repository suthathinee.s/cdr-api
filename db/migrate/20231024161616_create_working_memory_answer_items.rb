class CreateWorkingMemoryAnswerItems < ActiveRecord::Migration[7.0]
  def change
    create_table :working_memory_answer_items do |t|
      t.references :working_memory, foreign_key: {on_delete: :cascade}
      t.jsonb :answer
      t.references :working_memory_answer, foreign_key: {on_delete: :cascade}
      t.integer :answer_correct
      t.integer :answer_wrong

      t.timestamps
    end
  end
end
