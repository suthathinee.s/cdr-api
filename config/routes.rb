Rails.application.routes.draw do
  resources :pipe_answer_items
  resources :working_memory_answer_items
  resources :working_memory_answers
  resources :pipe_answers
  resources :answer_types
  resources :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  # Defines the root path route ("/")
  # root "articles#index"

  scope 'api' do
    match "logins/login", to: "logins#login", via: [:options, :post]
    match "users/login", to: "users#login", via: [:options, :post]
    match 'logins/logout', to: 'logins#logout', via: [:options, :delete]
    match 'users/logout', to: 'users#logout', via: [:options, :delete]
    match 'users/get_session', to: 'users#get_session', via: [:options, :get]
    match 'users/get_profile', to: 'users#get_profile', via: [:options, :get]
    match 'create_by_admin/create_working_memory', to: 'create_by_admin#create_working_memory', via: [:options, :post]
    match 'create_by_admin/create_pipe', to: 'create_by_admin#create_pipe', via: [:options, :post]
    match 'create_by_admin/destroy_working_memory', to: 'create_by_admin#destroy_working_memory', via: [:options, :delete]
    match 'create_by_admin/destroy_pipe', to: 'create_by_admin#destroy_pipe', via: [:options, :delete]
    match 'create_by_admin/destroy_game', to: 'create_by_admin#destroy_game', via: [:options, :delete]
    match 'create_by_admin/get_all_series', to: 'create_by_admin#get_all_series', via: [:options, :get]
    match 'create_by_admin/get_patients_imformation', to: 'create_by_admin#get_patients_imformation', via: [:options, :get]
    match 'create_by_admin/create_game_state', to: 'create_by_admin#create_game_state', via: [:options, :post]
    match 'create_by_admin/get_none_state', to: 'create_by_admin#get_none_state', via: [:options, :get]
    match 'create_by_admin/create_game_series', to: 'create_by_admin#create_game_series', via: [:options, :post]
    match 'create_by_admin/update_game_series/:id', to: 'create_by_admin#update_game_series', via: [:options, :put]
    match 'working_memories/none_using', to: 'working_memories#none_using', via: [:options, :get]
    match 'pipes/none_using', to: 'pipes#none_using', via: [:options, :get]
    match 'create_by_admin/get_working_memory_answer_report', to: 'create_by_admin#get_working_memory_answer_report', via: [:options, :get]
    match 'create_by_admin/get_game_answer', to: 'create_by_admin#get_game_answer', via: [:options, :get]
    match 'create_by_admin/get_game_answer/:id', to: 'create_by_admin#get_game_answer', via: [:options, :get]
    match 'create_by_admin/destroy_game_series/:id', to: 'create_by_admin#destroy_game_series', via: [:options, :delete]
    match "logins/get_expries", to: "logins#get_expries", via: [:options, :get]
    match 'create_by_admin/set_zero', to: 'create_by_admin#set_zero', via: [:options, :delete]
    match 'create_by_admin/create_game_state_with_items_and_series', to: 'create_by_admin#create_game_state_with_items_and_series', via: [:options, :post]
    match 'create_by_admin/get_none_series', to: 'create_by_admin#get_none_series', via: [:options, :get]
    %i[
      logins
      assignment_answers
      assignments
      doctors
      element_types
      elements
      game_types
      patients
      pipe_dimensions
      pipe_items
      pipe_serie_stages
      pipe_series
      pipes
      statuses
      test_set_items
      test_sets
      working_memory_serie_stages
      working_memory_series
      working_memory_sizes
      working_memory_items
      working_memories
      assignment_items
      users
      role_permissions
      genders
      careers
      answer_types
      pipe_answer_items
      working_memory_answer_items
      pipe_answers
      working_memory_answers
      create_by_admin
    ].each do |res|
      match res.to_s, to: "#{res}#index", via: [:options]
      match "#{res}/new", to: "#{res}#new", via: %i[options get]
      match "#{res}/:id", to: "#{res}#show", via: [:options]
      resources res
    end
  end

end
