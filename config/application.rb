require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module CdrApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    # timezone
    config.time_zone = "Bangkok" # Your local time zone
    config.active_record.default_timezone = :local
    config.active_record.time_zone_aware_attributes = false

    # hosts
    config.hosts << /.*/

    # active_record belongs_to required by default
    config.active_record.belongs_to_required_by_default = false

    # Hack for allowing SVG files. While this hack is here, we should **not**
    ActiveStorage::Engine.config
    .active_storage
    .content_types_to_serve_as_binary
    .delete('image/svg+xml')

    # # local env
    # config.before_configuration do
    #   env_file = File.join(Rails.root, 'config', 'local_env.yml')
    #   YAML.load(File.open(env_file)).each do |key, value|
    #     ENV[key.to_s] = value
    #   end if File.exists?(env_file)
    # end

    #config email server
    # config.action_mailer.default_options = {from: 'no-reply@gemail.com'}
    # config.action_mailer.delivery_method = :smtp
    # config.action_mailer.smtp_settings = {
    # address:              'smtp.gmail.com',
    # domain:               'smtp.gmail.com',  # หรือ 'gmail.com' หากใช้บัญชี Gmail
    # user_name:            'riseplus.technology@gmail.com',
    # password:             'nhnpiibbbgvjuxaf',
    # port:                 587,
    # authentication:       'login',
    # enable_starttls_auto: true,
    # ssl:                  false
    # }
    # config.action_mailer.perform_deliveries = true
    # config.action_mailer.default_options = {from: 'ABC KOI FARM <abckoifarm.official@gmail.com>'}
  end
end
