class RolePermissionsController < ApplicationController
  before_action :set_role_permission, only: %i[ show update destroy ]

  # GET /role_permissions
  def index
    @role_permissions = RolePermission.search params

    render json: @role_permissions
  end

  # GET /role_permissions/1
  def show
    render json: @role_permission
  end

  # POST /role_permissions
  def create
    @role_permission = RolePermission.new(role_permission_params)

    if @role_permission.save
      render json: @role_permission, status: :created, location: @role_permission
    else
      render json: @role_permission.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /role_permissions/1
  def update
    if @role_permission.update(role_permission_params)
      render json: @role_permission
    else
      render json: @role_permission.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /role_permissions/1
  def destroy
    @role_permission.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role_permission
      @role_permission = RolePermission.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Role Permission not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def role_permission_params
      params.require(:role_permission).permit(:name)
    end
end
