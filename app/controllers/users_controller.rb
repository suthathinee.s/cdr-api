class UsersController < ApplicationController
  before_action :set_user, only: %i[ show update destroy ]

  # GET /users
  def index
    @users = User.search params

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /users/1
  def destroy
    # Find the user
    @user = User.find(params[:id])

    # Find the associated doctor using doctor_id
    doctor = Doctor.find_by(id: @user.doctor_id)

    has_in_assignment = Assignment.find_by(doctor_id: @user.doctor_id)

    if has_in_assignment
      render json: { errors: "Cannot delete doctor, because doctor has in assignment." }, status: 404
      return
    end

    # Destroy the user
    @user.destroy

     # Print the associated users for debugging
     puts "Associated Users: #{doctor.users.inspect}" if doctor

     # If the associated doctor is found, destroy it
     if doctor
       doctor.destroy
     end

    # Render a response
    render json: { message: 'User and associated doctor destroyed successfully' }
  end

  # POST /logins
  def login
    # Initialize a new Login instance and set the username attribute
    if params[:username].blank? || params[:password].blank?
      render json: {erros: "Please check username, password."}, status: 401
    end
    encrypt_password = User.encrypt(params[:password])
    user = User.find_by(username: params[:username], password: encrypt_password)
    if user
      token = Login.create_token(username: user.username)
      render json: { token: token, role_id: user.role_permission_id, doctor_id: user.doctor_id }
    else
      render json: { errors: "Invalid username and password." }, status: 401
    end
  end

  def get_session
    session = params["session"]
    if params["doctor_obj"]
      doctor = params["doctor_obj"]
    end
    p "-----------#{session}"
    result = {
      username: session['username'],
      role_id: session['role_id'],
      doctor_id: session['doctor_id'], 
      doctor_obj: doctor.blank? ? nil : doctor
    } 
    render json: result
  end

  def get_profile
    session = params["session"]
    if params["doctor_obj"]
      doctor = params["doctor_obj"]
      if doctor.role_permission
        result = { 
          id: doctor.id,
          first_name: Doctor.decode_name(doctor.first_name.to_s),
          last_name: Doctor.decode_name(doctor.last_name.to_s),
          tel: doctor.tel,
          role_permission_id: doctor.role_permission_id,
          role_permission_name: doctor.role_permission.name
        } 
        render json: result
      else
        render json: { errors: "Role permission not found for the doctor" }, status: 404
      end
    else
      render json: { errors: "Invalid, cannot get profile." }, status: 404
    end
  end
  

  # DELETE /logins/logout
  def logout
    if request.headers['Authorization'].present?
      token = request.headers['Authorization'].split(' ').last
      RedisController.del_redis(key: token) # Delete the token from Redis cache
      render json: { message: 'Logged out successfully' }
    else
      render json: { error: 'No Authorization token provided' }, status: :unauthorized
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation, :doctor_id, :role_permission_id)
    end
end
