class PipeDimensionsController < ApplicationController
  before_action :set_pipe_dimension, only: %i[ show update destroy ]

  # GET /pipe_dimensions
  def index
    @pipe_dimensions = PipeDimension.search params

    render json: @pipe_dimensions
  end

  # GET /pipe_dimensions/1
  def show
    render json: @pipe_dimension
  end

  # POST /pipe_dimensions
  def create
    @pipe_dimension = PipeDimension.new(pipe_dimension_params)

    if @pipe_dimension.save
      render json: @pipe_dimension, status: :created, location: @pipe_dimension
    else
      render json: @pipe_dimension.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pipe_dimensions/1
  def update
    if @pipe_dimension.update(pipe_dimension_params)
      render json: @pipe_dimension
    else
      render json: @pipe_dimension.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /pipe_dimensions/1
  def destroy
    @pipe_dimension.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pipe_dimension
      @pipe_dimension = PipeDimension.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Pipe Answer not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def pipe_dimension_params
      params.require(:pipe_dimension).permit(:row, :col, :name)
    end
end
