class GameTypesController < ApplicationController
  before_action :set_game_type, only: %i[ show update destroy ]

  # GET /game_types
  def index
    @game_types = GameType.search params

    render json: @game_types
  end

  # GET /game_types/1
  def show
    render json: @game_type
  end

  # POST /game_types
  def create
    if game_type_params[:name].blank?
      render json: { error: 'Game Type name not found' }, status: 400
      return
    end

    @game_type = GameType.new(game_type_params)

    if @game_type.save
      render json: @game_type, status: :created, location: @game_type
    else
      render json: @game_type.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /game_types/1
  def update
    if game_type_params[:name].blank?
      render json: { error: 'Game Type name not found' }, status: 400
      return
    end
    
    if @game_type.update(game_type_params)
      render json: @game_type
    else
      render json: @game_type.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /game_types/1
  def destroy
    @game_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game_type
      @game_type = GameType.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Game Type not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def game_type_params
      params.require(:game_type).permit(:name)
    end
end
