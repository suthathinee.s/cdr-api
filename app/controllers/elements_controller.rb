class ElementsController < ApplicationController
  before_action :set_element, only: %i[ show update destroy ]

  # GET /elements
  def index
    @elements = Element.search params

    render json: @elements
  end

  # GET /elements/1
  def show
    render json: @element
  end

  # POST /elements
  def create
    element_type = ElementType.find_by(id: element_params[:element_type_id])
    if element_type.blank?
      render json: { error: 'Element Type not found' }, status: 400
      return
    end

    game_type = GameType.find_by(id: element_params[:game_type_id])
    if game_type.blank?
      render json: { error: 'Game Type not found' }, status: 400
      return
    end

    @element = Element.new(element_params)

    if @element.save
      render json: @element, status: :created, location: @element
    else
      render json: @element.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /elements/1
  def update
    element_type = ElementType.find_by(id: element_params[:element_type_id])
    if element_type.blank?
      render json: { error: 'Element Type not found' }, status: 400
      return
    end

    game_type = GameType.find_by(id: element_params[:game_type_id])
    if game_type.blank?
      render json: { error: 'Game Type not found' }, status: 400
      return
    end

    if @element.update(element_params)
      render json: @element
    else
      render json: @element.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /elements/1
  def destroy
    @element.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_element
      @element = Element.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Element not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def element_params
      params.require(:element).permit(:id, :new_image, :element_type_id, :game_type_id)
    end
end
