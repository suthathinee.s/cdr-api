class WorkingMemorySeriesController < ApplicationController
  before_action :set_working_memory_series, only: %i[show update destroy]

  # GET /working_memory_series
  def index
    @working_memory_series = WorkingMemorySeries.search(params)
    render json: @working_memory_series
  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Working Memory Series not found' }, status: :not_found
  end

  # GET /working_memory_series/1
  def show
    render json: @working_memory_series
  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Working Memory Series not found' }, status: :not_found
  end

  # POST /working_memory_series
  def create
    status = Status.find_by(id: working_memory_series_params[:status_id])
    game_type_id = 1

    if status.blank?
      render json: { error: 'Status not found' }, status: 400
      return
    end

    @working_memory_series = WorkingMemorySeries.new(working_memory_series_params.merge(game_type_id: game_type_id))

    if @working_memory_series.save
      render json: @working_memory_series, status: :created, location: @working_memory_series
    else
      render json: @working_memory_series.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /working_memory_series/1
  def update
    status = Status.find_by(id: working_memory_series_params[:status_id])
    if status.blank?
      render json: { error: 'Status not found' }, status: 400
      return
    end

    if @working_memory_series.update(working_memory_series_params)
      render json: @working_memory_series
    else
      render json: @working_memory_series.errors, status: :unprocessable_entity
      return
    end

  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Working Memory Series not found' }, status: :not_found
  end

  # DELETE /working_memory_series/1
  def destroy
    @working_memory_series.destroy
  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Working Memory Series not found' }, status: :not_found
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_working_memory_series
      @working_memory_series = WorkingMemorySeries.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Working Memory Series not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def working_memory_series_params
      params.require(:working_memory_series).permit(:name, :status_id, :game_type_id, new_working_memory_serie_stages: [:working_memory_id, :order_no])
    end
end
