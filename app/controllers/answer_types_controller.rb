class AnswerTypesController < ApplicationController
  before_action :set_answer_type, only: %i[ show update destroy ]

  # GET /answer_types
  def index
    @answer_types = AnswerType.search params

    render json: @answer_types
  end

  # GET /answer_types/1
  def show
    render json: @answer_type
  end

  # POST /answer_types
  def create
    @answer_type = AnswerType.new(answer_type_params)

    if @answer_type.save
      render json: @answer_type, status: :created, location: @answer_type
    else
      render json: @answer_type.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /answer_types/1
  def update
    if @answer_type.update(answer_type_params)
      render json: @answer_type
    else
      render json: @answer_type.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /answer_types/1
  def destroy
    @answer_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_answer_type
      @answer_type = AnswerType.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Answer type not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def answer_type_params
      params.require(:answer_type).permit(:name)
    end
end
