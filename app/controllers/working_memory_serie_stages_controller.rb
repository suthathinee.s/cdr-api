class WorkingMemorySerieStagesController < ApplicationController
  before_action :set_working_memory_serie_stage, only: %i[ show update destroy ]

  # GET /working_memory_serie_stages
  def index
    @working_memory_serie_stages = WorkingMemorySerieStage.search params

    render json: @working_memory_serie_stages
  end

  # GET /working_memory_serie_stages/1
  def show
    render json: @working_memory_serie_stage
  end

  # POST /working_memory_serie_stages
  def create
    working_memory_serie = WorkingMemorySerie.find_by(id: working_memory_serie_stage_params[:working_memory_serie_id])
    if working_memory_serie.blank?
      render json: { error: 'Working Memory Serie not found' }, status: :not_found
      return
    end

    working_memory = WorkingMemory.find_by(id: working_memory_serie_stage_params[:working_memory_id])
    if working_memory.blank?
      render json: { error: 'Working Memory not found' }, status: :not_found
      return
    end

    @working_memory_serie_stage = WorkingMemorySerieStage.new(working_memory_serie_stage_params)

    if @working_memory_serie_stage.save
      render json: @working_memory_serie_stage, status: :created, location: @working_memory_serie_stage
    else
      render json: @working_memory_serie_stage.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /working_memory_serie_stages/1
  def update
    working_memory_serie = WorkingMemorySerie.find_by(id: working_memory_serie_stage_params[:working_memory_serie_id])
    if working_memory_serie.blank?
      render json: { error: 'Working Memory Serie not found' }, status: :not_found
      return
    end

    working_memory = WorkingMemory.find_by(id: working_memory_serie_stage_params[:working_memory_id])
    if working_memory.blank?
      render json: { error: 'Working Memory not found' }, status: :not_found
      return
    end
    
    if @working_memory_serie_stage.update(working_memory_serie_stage_params)
      render json: @working_memory_serie_stage
    else
      render json: @working_memory_serie_stage.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /working_memory_serie_stages/1
  def destroy
    @working_memory_serie_stage.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_working_memory_serie_stage
      @working_memory_serie_stage = WorkingMemorySerieStage.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Working Memory Serie Stage not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def working_memory_serie_stage_params
      params.require(:working_memory_serie_stage).permit(:working_memory_serie_id, :working_memory_id, :order_no)
    end
end
