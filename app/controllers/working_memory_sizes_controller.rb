class WorkingMemorySizesController < ApplicationController
  before_action :set_working_memory_size, only: %i[ show update destroy ]

  # GET /working_memory_sizes
  def index
    @working_memory_sizes = WorkingMemorySize.search params

    render json: @working_memory_sizes
  end

  # GET /working_memory_sizes/1
  def show
    render json: @working_memory_size
  end

  # POST /working_memory_sizes
  def create
    @working_memory_size = WorkingMemorySize.new(working_memory_size_params)

    if @working_memory_size.save
      render json: @working_memory_size, status: :created, location: @working_memory_size
    else
      render json: @working_memory_size.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /working_memory_sizes/1
  def update
    if @working_memory_size.update(working_memory_size_params)
      render json: @working_memory_size
    else
      render json: @working_memory_size.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /working_memory_sizes/1
  def destroy
    @working_memory_size.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_working_memory_size
      @working_memory_size = WorkingMemorySize.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Working Memory Size not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def working_memory_size_params
      params.require(:working_memory_size).permit(:name, :size)
    end
end
