class StatusesController < ApplicationController
  before_action :set_status, only: %i[ show update destroy ]

  # GET /statuses
  def index
    @statuses = Status.search params

    render json: @statuses
  end

  # GET /statuses/1
  def show
    render json: @status
  end

  # POST /statuses
  def create
    if status_params[:name].blank?
      render json: { error: 'Status name not found' }, status: 400
      return
    end

    @status = Status.new(status_params)

    if @status.save
      render json: @status, status: :created, location: @status
    else
      render json: @status.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /statuses/1
  def update
    if status_params[:name].blank?
      render json: { error: 'Status name not found' }, status: 400
      return
    end
    
    if @status.update(status_params)
      render json: @status
    else
      render json: @status.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /statuses/1
  def destroy
    @status.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_status
      @status = Status.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Status not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def status_params
      params.require(:status).permit(:name)
    end
end
