class LoginsController < ApplicationController
  include TimeConversion
  before_action :set_login, only: %i[ show update destroy ]
  # GET /logins
  def get_expries
    if request.headers['Authorization'].present?
      token = request.headers['Authorization'].split(' ').last
      result = RedisController.get_redis(key: token)
      token_hms = seconds_to_hms(result)

      if result.to_i < 0
        render json: { status: false, error: 'Token is expired' }, status: :unauthorized
      else
        render json: { token: token, status: true,Time: token_hms  }
      end
    else
      render json: { error: 'No Authorization token provided' }, status: :unauthorized
    end
  end

  # POST /logins
  def login
    # Initialize a new Login instance and set the username attribute
    if params[:username].blank? || params[:password].blank?
      render json: {erros: "Please check username, password."}, status: 401
    end
    encrypt_password = User.encrypt(params[:password])
    user = User.find_by(username: params[:username], password: encrypt_password)
    if user
      token = Login.create_token(username: user.username)
      render json: { token: token }
    else
      render json: { errors: "Invalid username and password." }, status: 401
    end
  end

  # DELETE /logins/logout
  def logout
    if request.headers['Authorization'].present?
      token = request.headers['Authorization'].split(' ').last
      RedisController.del_redis(key: token) # Delete the token from Redis cache
      render json: { message: 'Logged out successfully' }
    else
      render json: { error: 'No Authorization token provided' }, status: :unauthorized
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_login
      @login = Login.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Login not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def login_params
      params.require(:login).permit(:username) # Adjust the permitted parameters accordingly
    end
end
