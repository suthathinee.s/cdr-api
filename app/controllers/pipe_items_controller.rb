class PipeItemsController < ApplicationController
  before_action :set_pipe_item, only: %i[ show update destroy ]

  # GET /pipe_items
  def index
    @pipe_items = PipeItem.search params

    render json: @pipe_items
  end

  # GET /pipe_items/1
  def show
    render json: @pipe_item
  end

  # POST /pipe_items
  def create
    pipe = Pipe.find_by(id: pipe_item_params[:pipe_id])
    if pipe.blank?
      render json: { error: 'Pipe not found' }, status: :not_found
      return
    end

    element = Element.find_by(id: pipe_item_params[:element_id])
    if element.blank?
      render json: { error: 'Element not found' }, status: :not_found
      return
    end

    @pipe_item = PipeItem.new(pipe_item_params)

    if @pipe_item.save
      render json: @pipe_item, status: :created, location: @pipe_item
    else
      render json: @pipe_item.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /pipe_items/1
  def update
    pipe = Pipe.find_by(id: pipe_item_params[:pipe_id])
    if pipe.blank?
      render json: { error: 'Pipe not found' }, status: :not_found
      return
    end

    element = Element.find_by(id: pipe_item_params[:element_id])
    if element.blank?
      render json: { error: 'Element not found' }, status: :not_found
      return
    end
    
    if @pipe_item.update(pipe_item_params)
      render json: @pipe_item
    else
      render json: @pipe_item.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /pipe_items/1
  def destroy
    @pipe_item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pipe_item
      @pipe_item = PipeItem.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Pipe Answer not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def pipe_item_params
      params.require(:pipe_item).permit(:pipe_id, :element_id, :rotation, :row_position, :col_position)
    end
end
