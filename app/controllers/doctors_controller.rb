class DoctorsController < ApplicationController
  before_action :set_doctor, only: %i[ show update destroy ]

  # GET /doctors
  def index
    @doctors = Doctor.search params

    render json: @doctors
  end

  # GET /doctors/1
  def show
    render json: @doctor
  end

  # POST /doctors
  def create
    role_permission = RolePermission.find_by(id: doctor_params[:role_permission_id])
    if role_permission.blank?
      render json: { error: 'Role Permission not found' }, status: :not_found
      return
    end

    username = User.find_by(username: doctor_params[:username])
    if username.present?
      render json: { error: 'Username already exists' }, status: :unprocessable_entity
      return
    end

    gender = Gender.find_by(id: doctor_params[:gender_id])

    if gender.blank?
      render json: { error: 'Gender not found' }, status: :not_found
      return
    end

    if doctor_params[:password] != doctor_params[:password_confirmation]
      render json: { error: 'Password and password confirmation do not match' }, status: :unprocessable_entity
      return
    end

    tel = doctor_params[:tel]
    if !valid_phone_number?(tel)
      render json: { error: 'Tel must be a valid phone number with up to 10 digits' }, status: :unprocessable_entity
      return
    end
    
    years_of_education = doctor_params[:years_of_education]
    if !valid_years_of_education?(years_of_education)
      render json: { error: 'Years of education must be a valid date' }, status: :unprocessable_entity
      return
    end

    if params[:doctor].present?
      @doctor = Doctor.new(doctor_params) # Exclude gender
  
      @doctor.first_name = Doctor.encode_name(doctor_params[:first_name])
      @doctor.last_name = Doctor.encode_name(doctor_params[:last_name])
  
      if @doctor.save
        render json: @doctor, status: :created, location: @doctor
      else
        render json: @doctor.errors, status: :unprocessable_entity
      end
    else
      render json: { error: 'Invalid parameters for doctor' }, status: :bad_request
    end
  end

  # PATCH/PUT /doctors/1
  def update
    role_permission = RolePermission.find_by(id: doctor_params[:role_permission_id])
    if role_permission.blank?
      render json: { error: 'Role Permission not found' }, status: :not_found
      return
    end

    tel = doctor_params[:tel]
    if !valid_phone_number?(tel)
      render json: { error: 'Tel must be a valid phone number with up to 10 digits' }, status: :unprocessable_entity
      return
    end

    years_of_education = doctor_params[:years_of_education]
    if !valid_years_of_education?(years_of_education)
      render json: { error: 'Years of education must be a valid date' }, status: :unprocessable_entity
      return
    end
    
    if @doctor.update(doctor_update_params)
      # Do not decode the names here, keep them as they are
      render json: @doctor, status: :ok
    else
      render json: @doctor.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /doctors/1
  def destroy
    @doctor.destroy
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_doctor
      @doctor = Doctor.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Doctor not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def doctor_params
      params.require(:doctor).permit(
        :first_name, :last_name, :tel, :role_permission_id, 
        :birthday, :years_of_education, :gender_id, 
        :username, :password, :password_confirmation
      )
    end

    def doctor_update_params
      params[:doctor][:first_name] = Doctor.encode_name(params[:doctor][:first_name]) # Encode the names
      params[:doctor][:last_name] = Doctor.encode_name(params[:doctor][:last_name]) # Encode the names
      
      params.require(:doctor).permit(
        :first_name, :last_name, :tel, :role_permission_id, 
        :birthday, :years_of_education, :gender_id, 
        :username, :password, :password_confirmation,
        :doctor_id # Add doctor_id to the permitted parameters
      )
    end
    
    def valid_phone_number?(tel)
      phone_number_pattern = /\A\d{10}\z/
      !tel.nil? && tel.to_s.match?(phone_number_pattern)
    end

    def valid_years_of_education?(years)
      begin
        Date.parse(years)
        return true
      rescue ArgumentError
        return false
      end
    end
    
end
