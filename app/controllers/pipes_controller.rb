class PipesController < ApplicationController
  before_action :set_pipe, only: %i[ show update destroy ]

  # GET /pipes/none-using
  def none_using
    pipe_not_in_series = Pipe.left_outer_joins(:pipe_serie_stages)
      .where(pipe_serie_stages: { id: nil })
      .order(id: :asc) 

    if pipe_not_in_series.empty?
      render json: { message: 'No orphaned pipe records found' }, status: :ok
    else
      render json: pipe_not_in_series, status: :ok
    end
  end

  # GET /pipes
  def index
    @pipes = Pipe.search params

    render json: @pipes
  end

  # GET /pipes/1
  def show
    render json: @pipe
  end

  # POST /pipes
  def create
    pipe_dimension = PipeDimension.find_by(id: pipe_params[:pipe_dimension_id])
    if pipe_dimension.blank?
      render json: { error: 'Pipe Dimension not found' }, status: :not_found
      return
    end

    status = Status.find_by(id: pipe_params[:status_id])
    if status.blank?
      render json: { error: 'Status not found' }, status: :not_found
      return
    end

    if pipe_params[:new_pipe_items].count != pipe_dimension.row * pipe_dimension.col
      render json: { error: "Pipe Items must have exactly #{pipe_dimension.row * pipe_dimension.col} items" }, status: :unprocessable_entity
      return
    end

    max_click = pipe_params[:max_click]
    if max_click.blank?
      render json: { error: 'Max click not found' }, status: :not_found
      return
    end
    if !max_click.is_a?(Integer)
      render json: { error: 'Max click must be an integer' }, status: :unprocessable_entity
      return
    end 

    max_times = pipe_params[:max_times]
    if max_times.blank?
      render json: { error: 'Max Times not found' }, status: :not_found
      return
    end
    if !max_times.is_a?(Integer)
      render json: { error: 'Max Times must be an integer' }, status: :unprocessable_entity
      return
    end   

    @pipe = Pipe.new(pipe_params)

    if @pipe.save
      render json: @pipe, status: :created, location: @pipe
    else
      render json: @pipe.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /pipes/1
  def update
    pipe_dimension = PipeDimension.find_by(id: pipe_params[:pipe_dimension_id])
    if pipe_dimension.blank?
      render json: { error: 'Pipe Dimension not found' }, status: :not_found
      return
    end

    max_click = pipe_params[:max_click]
    if max_click.blank?
      render json: { error: 'Max click not found' }, status: :not_found
      return
    end
    if !max_click.is_a?(Integer)
      render json: { error: 'Max click must be an integer' }, status: :unprocessable_entity
      return
    end 

    max_times = pipe_params[:max_times]
    if max_times.blank?
      render json: { error: 'Max Times not found' }, status: :not_found
      return
    end
    if !max_times.is_a?(Integer)
      render json: { error: 'Max Times must be an integer' }, status: :unprocessable_entity
      return
    end  

    status = Status.find_by(id: pipe_params[:status_id])
    if status.blank?
      render json: { error: 'Status not found' }, status: :not_found
      return
    end

    if @pipe.update(pipe_params)
      render json: @pipe
    else
      render json: @pipe.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /pipes/1
  def destroy
    @pipe.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pipe
      @pipe = Pipe.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Pipe not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def pipe_params
      params.require(:pipe).permit(:name, :max_click, :max_times, :pipe_dimension_id, :status_id, new_pipe_items: [:element_id, :rotation, :row_position, :col_position])
    end    
end
