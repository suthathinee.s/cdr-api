class WorkingMemoryAnswersController < ApplicationController
  before_action :set_working_memory_answer, only: %i[ show update destroy ]

  # GET /working_memory_answers
  def index
    @working_memory_answers = WorkingMemoryAnswer.search params

    render json: @working_memory_answers
  end

  # GET /working_memory_answers/1
  def show
    render json: @working_memory_answer
  end

  # POST /working_memory_answers
  def create
    assignment = Assignment.find_by(id: working_memory_answer_params[:assignment_id])
    if assignment.blank?
      render json: { error: 'Assignment not found' }, status: :not_found
      return
    end

    assignment_item = AssignmentItem.find_by(id: working_memory_answer_params[:assignment_item_id])
    if assignment_item.blank?
      render json: { error: 'Assignment Item not found' }, status: :not_found
      return
    end

    @working_memory_answer = WorkingMemoryAnswer.new(working_memory_answer_params)
    @working_memory_answer.start_date = DateTime.current
    if @working_memory_answer.save
      render json: @working_memory_answer, status: :created, location: @working_memory_answer
    else
      render json: @working_memory_answer.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /working_memory_answers/1
  def update
    assignment = Assignment.find_by(id: working_memory_answer_params[:assignment_id])
    if assignment.blank?
      render json: { error: 'Assignment not found' }, status: :not_found
      return
    end

    assignment_item = AssignmentItem.find_by(id: working_memory_answer_params[:assignment_item_id])
    if assignment_item.blank?
      render json: { error: 'Assignment Item not found' }, status: :not_found
      return
    end

    if @working_memory_answer.update(working_memory_answer_params)
      render json: @working_memory_answer
    else
      render json: @working_memory_answer.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /working_memory_answers/1
  def destroy
    @working_memory_answer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_working_memory_answer
      @working_memory_answer = WorkingMemoryAnswer.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Working Memory Answer not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    # def working_memory_answer_params
    #   params.require(:working_memory_answer).permit(:assignment_id, :assignment_item_id, :answer_correct, :answer_wrong, new_working_memory_answer_items: [:working_memory_id, :answer, :answer_correct, :answer_wrong])
    # end
    def working_memory_answer_params
      permitted = params.require(:working_memory_answer).permit(
        :assignment_id,
        :assignment_item_id,
        :answer_correct,
        :answer_wrong,
        :start_date,
        :end_date,
        new_working_memory_answer_items: [
          :working_memory_id, :answer_correct, :answer_wrong,
          answer: {},
          
        ]
      )
  
      # Ensure the "answer" hash has keys "1", "2", and "3" with string values
      unless permitted[:new_working_memory_answer_items].blank?
        answer = permitted[:new_working_memory_answer_items][0][:answer]
        answer.transform_keys!(&:to_s)  # Convert keys to strings
        answer.transform_values!(&:to_s) # Convert values to strings
        permitted[:new_working_memory_answer_items][0][:answer] = answer
      end
  
      permitted
    end
    
    
end
