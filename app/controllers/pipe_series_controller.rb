class PipeSeriesController < ApplicationController
  before_action :set_pipe_series, only: %i[show update destroy]

  # GET /pipe_series
  def index
    @pipe_series = PipeSeries.search(params)
    render json: @pipe_series
  end

  # GET /pipe_series/1
  def show
    render json: @pipe_series
  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Pipe Series not found' }, status: :not_found
  end

  # POST /pipe_series
  def create
    status = Status.find_by(id: pipe_series_params[:status_id])
    game_type_id = 2

    if status.blank?
      render json: { error: 'Status not found' }, status: :not_found
      return
    end

    @pipe_series = PipeSeries.new(pipe_series_params.merge(game_type_id: game_type_id))

    if @pipe_series.save
      render json: @pipe_series, status: :created, location: @pipe_series
    else
      render json: @pipe_series.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /pipe_series/1
  def update
    status = Status.find_by(id: pipe_series_params[:status_id])
    if status.blank?
      render json: { error: 'Status not found' }, status: :not_found
      return
    end

    if @pipe_series.update(pipe_series_params)
      render json: @pipe_series
    else
      render json: @pipe_series.errors, status: :unprocessable_entity
      return
    end
  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Pipe Series not found' }, status: :not_found
  end

  # DELETE /pipe_series/1
  def destroy
    @pipe_series.destroy
  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Pipe Series not found' }, status: :not_found
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pipe_series
      @pipe_series = PipeSeries.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Pipe Series not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def pipe_series_params
      params.require(:pipe_series).permit(:name, :status_id, :game_type_id, new_pipe_serie_stages: [:pipe_id, :order_no])
    end
end
