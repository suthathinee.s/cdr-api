class WorkingMemoryAnswerItemsController < ApplicationController
  before_action :set_working_memory_answer_item, only: %i[ show update destroy ]

  # GET /working_memory_answer_items
  def index
    @working_memory_answer_items = WorkingMemoryAnswerItem.search params

    render json: @working_memory_answer_items
  end

  # GET /working_memory_answer_items/1
  def show
    render json: @working_memory_answer_item
  end

  # POST /working_memory_answer_items
  def create
    working_memory = WorkingMemory.find_by(id: working_memory_answer_item_params[:working_memory_id])
    if working_memory.blank?
      render json: { error: 'Working Memory not found' }, status: :not_found
      return
    end

    working_memory_answer = WorkingMemoryAnswer.find_by(id: working_memory_answer_item_params[:working_memory_answer_id])
    if working_memory_answer.blank?
      render json: { error: 'Working Memory Answer not found' }, status: :not_found
      return
    end

    @working_memory_answer_item = WorkingMemoryAnswerItem.new(working_memory_answer_item_params)

    if @working_memory_answer_item.save
      render json: @working_memory_answer_item, status: :created, location: @working_memory_answer_item
    else
      render json: @working_memory_answer_item.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /working_memory_answer_items/1
  def update
    working_memory = WorkingMemory.find_by(id: working_memory_answer_item_params[:working_memory_id])
    if working_memory.blank?
      render json: { error: 'Working Memory not found' }, status: :not_found
      return
    end

    working_memory_answer = WorkingMemoryAnswer.find_by(id: working_memory_answer_item_params[:working_memory_answer_id])
    if working_memory_answer.blank?
      render json: { error: 'Working Memory Answer not found' }, status: :not_found
      return
    end
    
    if @working_memory_answer_item.update(working_memory_answer_item_params)
      render json: @working_memory_answer_item
    else
      render json: @working_memory_answer_item.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /working_memory_answer_items/1
  def destroy
    @working_memory_answer_item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_working_memory_answer_item
      @working_memory_answer_item = WorkingMemoryAnswerItem.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Working Memory Answer Item not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def working_memory_answer_item_params
      params.require(:working_memory_answer_item).permit(:working_memory_id, :answer, :working_memory_answer_id, :answer_correct, :answer_wrong)
    end
end
