class CreateByAdminController < ApplicationController
  before_action :set_pipe, only: %i[ show update destroy ]
  
  def get_all_series
    @all_series = GetAllSeries.table_all_series params

    render json: @all_series
  end

  def get_patients_imformation
    @patients_imformation = GetAllSeries.patients_imformation params

    render json: @patients_imformation
  end

  def get_working_memory_answer_report
    @working_memory_answer_report = GetAllSeries.working_memory_answer_report params

    render json: @working_memory_answer_report
  end

  def get_game_answer
    game_type_id_param = params[:game_type_id]
  
    case game_type_id_param.to_i
    when 1
      @working_memory_answers = WorkingMemoryAnswer.search params
      render json: @working_memory_answers
    when 2
      @pipe_answers = PipeAnswer.search params
      render json: @pipe_answers
    else
      render json: { error: 'Invalid game_type_id' }, status: :bad_request
    end
  end
  

  def create_game_series
    game_type_id_param = params[:game_type_id]

    if game_type_id_param == 1
      status = Status.find_by(id: working_memory_series_params[:status_id])
      if status.blank?
        render json: { error: 'Status not found' }, status: 400
        return
      end

      @working_memory_series = WorkingMemorySeries.new(working_memory_series_params)

      if @working_memory_series.save
        render json: @working_memory_series, status: :created, location: @working_memory_series
      else
        render json: @working_memory_series.errors, status: :unprocessable_entity
        return
      end
    end

    if game_type_id_param == 2
      status = Status.find_by(id: pipe_series_params[:status_id])
      if status.blank?
        render json: { error: 'Status not found' }, status: :not_found
        return
      end

      @pipe_series = PipeSeries.new(pipe_series_params)

      if @pipe_series.save
        render json: @pipe_series, status: :created, location: @pipe_series
      else
        render json: @pipe_series.errors, status: :unprocessable_entity
        return
      end
    end

  end

  def update_game_series
    game_type_id_param = params[:game_type_id]

    if game_type_id_param == 1
      @working_memory_series = WorkingMemorySeries.find(params[:id]) # Find the existing record

      status = Status.find_by(id: working_memory_series_params[:status_id])
      if status.blank?
        render json: { error: 'Status not found' }, status: 400
        return
      end

      if @working_memory_series.update(working_memory_series_params)
        render json: @working_memory_series
      else
        render json: @working_memory_series.errors, status: :unprocessable_entity
        return
      end
    end

    if game_type_id_param == 2
      @pipe_series = PipeSeries.find(params[:id]) # Find the existing record

      status = Status.find_by(id: pipe_series_params[:status_id])
      if status.blank?
        render json: { error: 'Status not found' }, status: :not_found
        return
      end

      if @pipe_series.update(pipe_series_params)
        render json: @pipe_series
      else
        render json: @pipe_series.errors, status: :unprocessable_entity
        return
      end
    end
  end

  def get_none_state
    game_type_id_param = params[:game_type_id]
  
    case game_type_id_param.to_i
    when 1
      working_memories_not_in_series = WorkingMemory.left_outer_joins(:working_memory_serie_stages)
        .where(working_memory_serie_stages: { id: nil })
        .order(id: :asc)
  
      if working_memories_not_in_series.empty?
        render json: { message: 'No orphaned working memory records found' }, status: :ok
      else
        render json: working_memories_not_in_series, status: :ok
      end
    when 2
      pipe_not_in_series = Pipe.left_outer_joins(:pipe_serie_stages)
        .where(pipe_serie_stages: { id: nil })
        .order(id: :asc)
  
      if pipe_not_in_series.empty?
        render json: { message: 'No orphaned pipe records found' }, status: :ok
      else
        render json: pipe_not_in_series, status: :ok
      end
    else
      render json: { error: 'Invalid game_type_id' }, status: :bad_request
    end
  end
  
  def create_working_memory
    created_working_memories = []  # Add this line
  
    conditions = [
      { working_memory_size_id: 1, max_times: 30, status_id: 2, name: 'Working Memory 1' },
      { working_memory_size_id: 1, max_times: 30, status_id: 2, name: 'Working Memory 2' },
      { working_memory_size_id: 2, max_times: 30, status_id: 2, name: 'Working Memory 3' },
      { working_memory_size_id: 2, max_times: 30, status_id: 2, name: 'Working Memory 4' },
      { working_memory_size_id: 3, max_times: 30, status_id: 2, name: 'Working Memory 5' },
      { working_memory_size_id: 3, max_times: 30, status_id: 2, name: 'Working Memory 6' },
      { working_memory_size_id: 4, max_times: 30, status_id: 2, name: 'Working Memory 7' },
      { working_memory_size_id: 4, max_times: 30, status_id: 2, name: 'Working Memory 8' },
      { working_memory_size_id: 5, max_times: 30, status_id: 2, name: 'Working Memory 9' },
      { working_memory_size_id: 5, max_times: 30, status_id: 2, name: 'Working Memory 10' },
      { working_memory_size_id: 6, max_times: 30, status_id: 2, name: 'Working Memory 11' },
      { working_memory_size_id: 6, max_times: 30, status_id: 2, name: 'Working Memory 12' },
      { working_memory_size_id: 7, max_times: 30, status_id: 2, name: 'Working Memory 13' },
      { working_memory_size_id: 7, max_times: 30, status_id: 2, name: 'Working Memory 14' }
    ]
  
    conditions.each do |condition|
      working_memory = WorkingMemory.new(
        working_memory_size_id: condition[:working_memory_size_id],
        max_times: condition[:max_times],
        name: condition[:name],
        status_id: condition[:status_id]
      )
  
      if working_memory.save
        created_working_memories << working_memory
      else
        render json: { error: working_memory.errors.full_messages }, status: :unprocessable_entity
        return
      end
    end
  
    render json: created_working_memories, status: :created
  end
  
  def create_pipe
    created_pipes = []  # Add this line
    conditions = [
      { pipe_dimension_id: 1, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 1' },
      { pipe_dimension_id: 2, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 2' },
      { pipe_dimension_id: 2, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 3' },
      { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 4' },
      { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 5' },
      { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 6' }
    ]

    conditions.each do |condition|
      pipe = Pipe.new(
        pipe_dimension_id: condition[:pipe_dimension_id],
        max_times: condition[:max_times],
        max_click: condition[:max_click],
        name: condition[:name],
        status_id: condition[:status_id]
      )

      if pipe.save
        created_pipes << pipe
      else
        render json: { error: pipe.errors.full_messages }, status: :unprocessable_entity
        return
      end
    end

    render json: created_pipes, status: :created
  end

  def destroy_game
    destroy_game_type_param = params[:game_type_id]

    case destroy_game_type_param.to_i
    when 1
        working_memories_not_in_series = WorkingMemory.left_outer_joins(:working_memory_serie_stages)
        .where(working_memory_serie_stages: { id: nil })

      if working_memories_not_in_series.empty?
        render json: { message: 'No orphaned working memory records found' }, status: :ok
      else
        WorkingMemoryItem.where(working_memory_id: working_memories_not_in_series.pluck(:id)).destroy_all

        working_memories_not_in_series.each do |working_memory|
          working_memory.destroy
        end

        render json: { message: 'Orphaned working memory records deleted successfully' }, status: :ok
      end

    when 2
        pipe_not_in_series = Pipe.left_outer_joins(:pipe_serie_stages)
        .where(pipe_serie_stages: { id: nil })

      if pipe_not_in_series.empty?
        render json: { message: 'No orphaned pipe records found' }, status: :ok
      else
        PipeItem.where(pipe_id: pipe_not_in_series.pluck(:id)).destroy_all

        pipe_not_in_series.each do |pipe|
          pipe.destroy
        end

        render json: { message: 'Orphaned pipe records deleted successfully' }, status: :ok
      end
    else
      working_memories_not_in_series = WorkingMemory.left_outer_joins(:working_memory_serie_stages)
        .where(working_memory_serie_stages: { id: nil })

      pipe_not_in_series = Pipe.left_outer_joins(:pipe_serie_stages)
        .where(pipe_serie_stages: { id: nil })

      if pipe_not_in_series.empty? && working_memories_not_in_series.empty?
        render json: { message: 'No orphaned working memory and pipe records found' }, status: :ok
      else
          WorkingMemoryItem.where(working_memory_id: working_memories_not_in_series.pluck(:id)).destroy_all
        working_memories_not_in_series.each do |working_memory|
          working_memory.destroy
        end
    
        PipeItem.where(pipe_id: pipe_not_in_series.pluck(:id)).destroy_all
        pipe_not_in_series.each do |pipe|
          pipe.destroy
        end

      render json: { message: 'Orphaned working memory and pipe record deleted successfully' }, status: :ok
      end
    end
  end

  def destroy_game_series
    game_type_param = params[:game_type_id]
  
    case game_type_param.to_i
    when 1
      @working_memory_series = WorkingMemorySeries.find_by(id: params[:id])
      if @working_memory_series
        @working_memory_series.destroy
        render json: { message: 'Working Memory Series deleted successfully' }, status: :ok
      else
        render json: { error: 'Working Memory Series not found' }, status: :not_found
      end
    when 2
      @pipe_series = PipeSeries.find_by(id: params[:id])
      if @pipe_series
        @pipe_series.destroy
        render json: { message: 'Pipe Series deleted successfully' }, status: :ok
      else
        render json: { error: 'Pipe Series not found' }, status: :not_found
      end
    else
      render json: { error: 'Invalid game_type_id' }, status: :bad_request
    end
  end
  
  def destroy_working_memory
    # Find working memory records that are not part of any working memory series
    working_memories_not_in_series = WorkingMemory.left_outer_joins(:working_memory_serie_stages)
      .where(working_memory_serie_stages: { id: nil })

    if working_memories_not_in_series.empty?
      render json: { message: 'No orphaned working memory records found' }, status: :ok
    else
      WorkingMemoryItem.where(working_memory_id: working_memories_not_in_series.pluck(:id)).destroy_all

      working_memories_not_in_series.each do |working_memory|
        working_memory.destroy
      end

      render json: { message: 'Orphaned working memory records deleted successfully' }, status: :ok
    end
  end

  def destroy_pipe
    # Find pipe records that are not part of any pipe series
    pipe_not_in_series = Pipe.left_outer_joins(:pipe_serie_stages)
      .where(pipe_serie_stages: { id: nil })

    if pipe_not_in_series.empty?
      render json: { message: 'No orphaned pipe records found' }, status: :ok
    else
      PipeItem.where(pipe_id: pipe_not_in_series.pluck(:id)).destroy_all

      pipe_not_in_series.each do |pipe|
        pipe.destroy
      end

      render json: { message: 'Orphaned pipe records deleted successfully' }, status: :ok
    end
  end

  def create_game_state
    create_game_type_param = params[:game_type_id]
    working_memory_ids = []
    pipe_ids = []

    created_working_memories = []  # Add this line
    created_pipes = []  # Add this line

    if create_game_type_param.nil?
      render json: { error: 'Game type is required' }, status: :unprocessable_entity
      return
    end
    
    if create_game_type_param == 1
      conditions_working_memory = [
        { working_memory_size_id: 1, max_times: 30, status_id: 2, name: 'Working Memory 1' },
        { working_memory_size_id: 1, max_times: 30, status_id: 2, name: 'Working Memory 2' },
        { working_memory_size_id: 2, max_times: 30, status_id: 2, name: 'Working Memory 3' },
        { working_memory_size_id: 2, max_times: 30, status_id: 2, name: 'Working Memory 4' },
        { working_memory_size_id: 3, max_times: 30, status_id: 2, name: 'Working Memory 5' },
        { working_memory_size_id: 3, max_times: 30, status_id: 2, name: 'Working Memory 6' },
        { working_memory_size_id: 4, max_times: 30, status_id: 2, name: 'Working Memory 7' },
        { working_memory_size_id: 4, max_times: 30, status_id: 2, name: 'Working Memory 8' },
        { working_memory_size_id: 5, max_times: 30, status_id: 2, name: 'Working Memory 9' },
        { working_memory_size_id: 5, max_times: 30, status_id: 2, name: 'Working Memory 10' },
        { working_memory_size_id: 6, max_times: 30, status_id: 2, name: 'Working Memory 11' },
        { working_memory_size_id: 6, max_times: 30, status_id: 2, name: 'Working Memory 12' },
        { working_memory_size_id: 7, max_times: 30, status_id: 2, name: 'Working Memory 13' },
        { working_memory_size_id: 7, max_times: 30, status_id: 2, name: 'Working Memory 14' }
      ]
  
      conditions_working_memory.each do |condition_working_memory|
        working_memory = WorkingMemory.new(
          working_memory_size_id: condition_working_memory[:working_memory_size_id],
          max_times: condition_working_memory[:max_times],
          name: condition_working_memory[:name],
          status_id: condition_working_memory[:status_id]
        )
  
        if working_memory.save
          created_working_memories << working_memory
        else
          render json: { error: working_memory.errors.full_messages }, status: :unprocessable_entity
          return
        end
      end
      render json: created_working_memories, status: :created
    end

    if create_game_type_param == 2
      conditions_pipe = [
        { pipe_dimension_id: 1, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 1' },
        { pipe_dimension_id: 2, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 2' },
        { pipe_dimension_id: 2, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 3' },
        { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 4' },
        { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 5' },
        { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 6' }
      ]
  
      conditions_pipe.each do |condition_pipe|
        pipe = Pipe.new(
          pipe_dimension_id: condition_pipe[:pipe_dimension_id],
          max_times: condition_pipe[:max_times],
          max_click: condition_pipe[:max_click],
          name: condition_pipe[:name],
          status_id: condition_pipe[:status_id]
        )
  
        if pipe.save
          created_pipes << pipe
        else
          render json: { error: pipe.errors.full_messages }, status: :unprocessable_entity
          return
        end
      end
      render json: created_pipes, status: :created
    end
    
  end  

  def set_zero
    AssignmentItem.destroy_all
    Assignment.destroy_all
    Patient.destroy_all
    User.destroy_all

    if User.all.empty? && Patient.all.empty? && Assignment.all.empty? && AssignmentItem.all.empty?
      Doctor.destroy_all
    end
    WorkingMemorySeries.destroy_all
    WorkingMemorySerieStage.destroy_all
    WorkingMemoryItem.destroy_all
    WorkingMemory.destroy_all
    PipeSeries.destroy_all
    PipeSerieStage.destroy_all
    PipeItem.destroy_all
    Pipe.destroy_all

    if Patient.all.empty? && Doctor.all.empty? && User.all.empty? && AssignmentItem.all.empty? && Assignment.all.empty? && WorkingMemorySeries.all.empty? && WorkingMemorySerieStage.all.empty? && WorkingMemory.all.empty? && WorkingMemoryItem.all.empty? && PipeSeries.all.empty? && PipeSerieStage.all.empty? && Pipe.all.empty? && PipeItem.all.empty?
      doctor = Doctor.new(first_name: Doctor.encode_name("sys"), last_name: Doctor.encode_name("admin"), gender_id: 1, years_of_education: "1990-01-01", birthday: "1990-01-01", tel: "0000000000", role_permission_id: 1)
      doctor.save
  
      user = User.new(username: 'sysadmin', password: 'sysadmin', password_confirmation: 'sysadmin', doctor_id: doctor.id, role_permission_id: 1)
      user.save
      render json: { message: 'All data has been set to zero' }, status: :ok
    else
      render json: { error: 'All data has not been set to zero' }, status: :unprocessable_entity
      return
    end
  end

  def create_game_state_with_items_and_series
    create_game_type_param = params[:game_type_id]
    working_memory_ids = []
    pipe_ids = []

    created_working_memories = []  # Add this line
    created_pipes = []  # Add this line

    if create_game_type_param.nil?
      render json: { error: 'Game type is required' }, status: :unprocessable_entity
      return
    end
    
    if create_game_type_param == 1
      conditions_working_memory = [
        { working_memory_size_id: 1, max_times: 30, status_id: 2, name: 'Working Memory 1', new_working_memory_items: [
          {   "element_id": 6,    "position": 1 },
          {   "element_id": 5,   "position": 2 },
          {   "element_id": 10,    "position": 3 }
          ]
        },
        { working_memory_size_id: 1, max_times: 30, status_id: 2, name: 'Working Memory 2', new_working_memory_items: [
          {   "element_id": 1,    "position": 1 },
          {   "element_id": 11,   "position": 2 },
          {   "element_id": 10,    "position": 3 }  
          ]
        },
        { working_memory_size_id: 2, max_times: 30, status_id: 2, name: 'Working Memory 3', new_working_memory_items: [
          {   "element_id": 8,    "position": 1 },
          {   "element_id": 3,   "position": 2 },
          {   "element_id": 14,    "position": 3 },
          {   "element_id": 11,   "position": 4 }  
          ]
        },
        { working_memory_size_id: 2, max_times: 30, status_id: 2, name: 'Working Memory 4',  new_working_memory_items: [
          {   "element_id": 11,    "position": 1 },
          {   "element_id": 9,   "position": 2 },
          {   "element_id": 2,    "position": 3 },
          {   "element_id": 12,   "position": 4 } 
          ]
        },
        { working_memory_size_id: 3, max_times: 30, status_id: 2, name: 'Working Memory 5', new_working_memory_items: [
          {   "element_id": 14,    "position": 1 },
          {   "element_id": 7,   "position": 2 },
          {   "element_id": 4,    "position": 3 },
          {   "element_id": 10,   "position": 4 },
          {   "element_id": 1,    "position": 5 }
          ]
        },
        { working_memory_size_id: 3, max_times: 30, status_id: 2, name: 'Working Memory 6', new_working_memory_items: [
          {   "element_id": 9,    "position": 1 },
          {   "element_id": 14,   "position": 2 },
          {   "element_id": 2,    "position": 3 },
          {   "element_id": 11,   "position": 4 },
          {   "element_id": 13,    "position": 5 }
          ]
        },
        { working_memory_size_id: 4, max_times: 30, status_id: 2, name: 'Working Memory 7', new_working_memory_items: [
          {   "element_id": 14,    "position": 1 },
          {   "element_id": 3,   "position": 2 },
          {   "element_id": 11,    "position": 3 },
          {   "element_id": 8,   "position": 4 },
          {   "element_id": 12,    "position": 5 },
          {   "element_id": 7,   "position": 6 }  
          ]
        },
        { working_memory_size_id: 4, max_times: 30, status_id: 2, name: 'Working Memory 8', new_working_memory_items: [
          {   "element_id": 5,    "position": 1 },
          {   "element_id": 2,   "position": 2 },
          {   "element_id": 11,    "position": 3 },
          {   "element_id": 10,   "position": 4 },
          {   "element_id": 7,    "position": 5 },
          {   "element_id": 13,   "position": 6 }
          ]
        },
        { working_memory_size_id: 5, max_times: 30, status_id: 2, name: 'Working Memory 9', new_working_memory_items: [
          {   "element_id": 3,    "position": 1 },
          {   "element_id": 13,   "position": 2 },
          {   "element_id": 7,    "position": 3 },
          {   "element_id": 14,   "position": 4 },
          {   "element_id": 2,    "position": 5 },
          {   "element_id": 9,   "position": 6 },
          {   "element_id": 12,    "position": 7 }  
          ]
        },
        { working_memory_size_id: 5, max_times: 30, status_id: 2, name: 'Working Memory 10', new_working_memory_items: [
          {   "element_id": 13,    "position": 1 },
          {   "element_id": 12,   "position": 2 },
          {   "element_id": 4,    "position": 3 },
          {   "element_id": 1,   "position": 4 },
          {   "element_id": 14,    "position": 5 },
          {   "element_id": 7,   "position": 6 },
          {   "element_id": 10,    "position": 7 }
          ]
        },
        { working_memory_size_id: 6, max_times: 30, status_id: 2, name: 'Working Memory 11', new_working_memory_items: [
          {   "element_id": 8,    "position": 1 },
          {   "element_id": 9,   "position": 2 },
          {   "element_id": 10,    "position": 3 },
          {   "element_id": 14,   "position": 4 },
          {   "element_id": 2,    "position": 5 },
          {   "element_id": 12,   "position": 6 },
          {   "element_id": 11,    "position": 7 },
          {   "element_id": 7,   "position": 8 }  
          ]
        },
        { working_memory_size_id: 6, max_times: 30, status_id: 2, name: 'Working Memory 12', new_working_memory_items: [
          {   "element_id": 13,    "position": 1 },
          {   "element_id": 1,   "position": 2 },
          {   "element_id": 7,    "position": 3 },
          {   "element_id": 12,   "position": 4 },
          {   "element_id": 2,    "position": 5 },
          {   "element_id": 10,   "position": 6 },
          {   "element_id": 11,    "position": 7 },
          {   "element_id": 6,   "position": 8 }  
          ]
        },
        { working_memory_size_id: 7, max_times: 30, status_id: 2, name: 'Working Memory 13', new_working_memory_items: [
          {   "element_id": 7,    "position": 1 },
          {   "element_id": 11,   "position": 2 },
          {   "element_id": 1,    "position": 3 },
          {   "element_id": 14,   "position": 4 },
          {   "element_id": 9,    "position": 5 },
          {   "element_id": 13,   "position": 6 },
          {   "element_id": 2,    "position": 7 },
          {   "element_id": 12,   "position": 8 },
          {   "element_id": 10,   "position": 9 }
          ]
        },
        { working_memory_size_id: 7, max_times: 30, status_id: 2, name: 'Working Memory 14', new_working_memory_items: [
          {   "element_id": 3,    "position": 1 },
          {   "element_id": 12,   "position": 2 },
          {   "element_id": 8,    "position": 3 },
          {   "element_id": 5,   "position": 4 },
          {   "element_id": 14,    "position": 5 },
          {   "element_id": 9,   "position": 6 },
          {   "element_id": 2,    "position": 7 },
          {   "element_id": 13,   "position": 8 },
          {   "element_id": 10,   "position": 9 }
          ]
        }
      ]
  
      conditions_working_memory.each do |condition_working_memory|
        working_memory = WorkingMemory.new(
          working_memory_size_id: condition_working_memory[:working_memory_size_id],
          max_times: condition_working_memory[:max_times],
          name: condition_working_memory[:name],
          status_id: condition_working_memory[:status_id],
          new_working_memory_items: condition_working_memory[:new_working_memory_items]
        )
  
        if working_memory.save
          created_working_memories << working_memory
        else
          render json: { error: working_memory.errors.full_messages }, status: :unprocessable_entity
          return
        end
      end
      
      working_memory_series = []

      created_working_memories.each do |created_working_memory|
        working_memory_ids << created_working_memory.id
      end
      
      conditions_working_memory_series = [
        {
          status_id: 1,
          name: 'Working Memory Series',
          new_working_memory_serie_stages: working_memory_ids.map.with_index do |id, index|
            { working_memory_id: id, order_no: index + 1 }
          end
        }
      ]
  
      conditions_working_memory_series.each do |condition_working_memory_serie|
        working_memory_serie = WorkingMemorySeries.new(
          name: condition_working_memory_serie[:name],
          status_id: condition_working_memory_serie[:status_id],
          new_working_memory_serie_stages: condition_working_memory_serie[:new_working_memory_serie_stages]
        )
  
        if working_memory_serie.save
          working_memory_series << working_memory_serie
        else
          render json: { error: working_memory_serie.errors.full_messages }, status: :unprocessable_entity
          return
        end
      end
      render json: working_memory_series, status: :created
    end

    if create_game_type_param == 2
      conditions_pipe = [
        { pipe_dimension_id: 1, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 1', new_pipe_items: [
          { "element_id": 15,  "rotation": 0,      "row_position": 0,  "col_position": 0 },
          { "element_id": 25,  "rotation": 0,      "row_position": 0,  "col_position": 1 },
          { "element_id": 15,  "rotation": 90,     "row_position": 0,  "col_position": 2 },
          
          { "element_id": 25,  "rotation": 90,     "row_position": 1,  "col_position": 0 },
          { "element_id": 25,  "rotation": 0,      "row_position": 1,  "col_position": 1 },
          { "element_id": 15,  "rotation": 180,    "row_position": 1,  "col_position": 2 },

          { "element_id": 15,  "rotation": 180,    "row_position": 2,  "col_position": 0 },
          { "element_id": 25,  "rotation": 0,      "row_position": 2,  "col_position": 1 },
          { "element_id": 25,  "rotation": 90,     "row_position": 2,  "col_position": 2 }
           
        ]},
        { pipe_dimension_id: 2, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 2', new_pipe_items: [
          { "element_id": 46,  "rotation": 0,    "row_position": 0,  "col_position": 0 },
          { "element_id": 25,  "rotation": 90,   "row_position": 0,  "col_position": 1 },
          { "element_id": 15,  "rotation": 0,    "row_position": 0,  "col_position": 2 },
          { "element_id": 25,  "rotation": 0,    "row_position": 0,  "col_position": 3 },
          
          { "element_id": 15,  "rotation": 270,  "row_position": 1,  "col_position": 0 },
          { "element_id": 25,  "rotation": 0,    "row_position": 1,  "col_position": 1 },
          { "element_id": 15,  "rotation": 180,  "row_position": 1,  "col_position": 2 },
          { "element_id": 35,  "rotation": 90,   "row_position": 1,  "col_position": 3 },
          
          { "element_id": 25,  "rotation": 90,   "row_position": 2,  "col_position": 0 },
          { "element_id": 25,  "rotation": 0,    "row_position": 2,  "col_position": 1 },
          { "element_id": 15,  "rotation": 0,    "row_position": 2,  "col_position": 2 },
          { "element_id": 35,  "rotation": 0,    "row_position": 2,  "col_position": 3 },
          
          { "element_id": 15,  "rotation": 180,  "row_position": 3,  "col_position": 0 },
          { "element_id": 49,  "rotation": 0,    "row_position": 3,  "col_position": 1 },
          { "element_id": 25,  "rotation": 0,    "row_position": 3,  "col_position": 2 },
          { "element_id": 15,  "rotation": 180,  "row_position": 3,  "col_position": 3 }
           
        ]},
        { pipe_dimension_id: 2, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 3', new_pipe_items: [
          { "element_id": 35,  "rotation": 0,      "row_position": 0,  "col_position": 0 },
          { "element_id": 25,  "rotation": 0,      "row_position": 0,  "col_position": 1 },
          { "element_id": 15,  "rotation": 0,      "row_position": 0,  "col_position": 2 },
          { "element_id": 46,  "rotation": 90,     "row_position": 0,  "col_position": 3 },
          
          { "element_id": 25,  "rotation": 90,     "row_position": 1,  "col_position": 0 },
          { "element_id": 15,  "rotation": 270,    "row_position": 1,  "col_position": 1 },
          { "element_id": 25,  "rotation": 0,      "row_position": 1,  "col_position": 2 },
          { "element_id": 35,  "rotation": 270,    "row_position": 1,  "col_position": 3 },
          
          { "element_id": 25,  "rotation": 90,     "row_position": 2,  "col_position": 0 },
          { "element_id": 25,  "rotation": 0,      "row_position": 2,  "col_position": 1 },
          { "element_id": 15,  "rotation": 90,     "row_position": 2,  "col_position": 2 },
          { "element_id": 49,  "rotation": 90,     "row_position": 2,  "col_position": 3 },
          
          { "element_id": 49,  "rotation": 180,    "row_position": 3,  "col_position": 0 },
          { "element_id": 15,  "rotation": 90,     "row_position": 3,  "col_position": 1 },
          { "element_id": 25,  "rotation": 90,     "row_position": 3,  "col_position": 2 },
          { "element_id": 15,  "rotation": 180,    "row_position": 3,  "col_position": 3 }
           
        ]},
        { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 4', new_pipe_items: [
          {"element_id": 46, "rotation": 0,  "row_position": 0,"col_position": 0},
          {"element_id": 15, "rotation": 90, "row_position": 0,"col_position": 1},
          {"element_id": 25, "rotation": 0,  "row_position": 0,"col_position": 2},
          {"element_id": 15, "rotation": 90, "row_position": 0,"col_position": 3},
          {"element_id": 15, "rotation": 90, "row_position": 0,"col_position": 4},

          {"element_id": 25, "rotation": 0,  "row_position": 1,"col_position": 0},
          {"element_id": 35, "rotation": 180,"row_position": 1,"col_position": 1},
          {"element_id": 25, "rotation": 90, "row_position": 1,"col_position": 2},
          {"element_id": 49, "rotation": 0,  "row_position": 1,"col_position": 3},
          {"element_id": 35, "rotation": 90, "row_position": 1,"col_position": 4},

          {"element_id": 15, "rotation": 270,"row_position": 2,"col_position": 0},
          {"element_id": 15, "rotation": 180,"row_position": 2,"col_position": 1},
          {"element_id": 15, "rotation": 0,  "row_position": 2,"col_position": 2},
          {"element_id": 15, "rotation": 90, "row_position": 2,"col_position": 3},
          {"element_id": 25, "rotation": 90, "row_position": 2,"col_position": 4},

          {"element_id": 25, "rotation": 0,  "row_position": 3,"col_position": 0},
          {"element_id": 15, "rotation": 0,  "row_position": 3,"col_position": 1},
          {"element_id": 25, "rotation": 0,  "row_position": 3,"col_position": 2},
          {"element_id": 35, "rotation": 180,"row_position": 3,"col_position": 3},
          {"element_id": 15, "rotation": 180,"row_position": 3,"col_position": 4},
          
          {"element_id": 25, "rotation": 0,  "row_position": 4, "col_position": 0},
          {"element_id": 46, "rotation": 270,"row_position": 4, "col_position": 1},
          {"element_id": 15, "rotation": 180,"row_position": 4, "col_position": 2},
          {"element_id": 15, "rotation": 180,"row_position": 4, "col_position": 3},
          {"element_id": 25, "rotation": 0,  "row_position": 4, "col_position": 4}
        ]},
        { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 5', new_pipe_items: [
          { "element_id": 25,  "rotation": 90,     "row_position": 0,  "col_position": 0 },
          { "element_id": 15,  "rotation": 270,    "row_position": 0,  "col_position": 1 },
          { "element_id": 49,  "rotation": 0,      "row_position": 0,  "col_position": 2 },
          { "element_id": 25,  "rotation": 0,      "row_position": 0,  "col_position": 3 },
          { "element_id": 15,  "rotation": 180,    "row_position": 0,  "col_position": 4 },
          
          { "element_id": 15,  "rotation": 270,    "row_position": 1,  "col_position": 0 },
          { "element_id": 25,  "rotation": 0,      "row_position": 1,  "col_position": 1 },
          { "element_id": 35,  "rotation": 180,    "row_position": 1,  "col_position": 2 },
          { "element_id": 15,  "rotation": 90,     "row_position": 1,  "col_position": 3 },
          { "element_id": 35,  "rotation": 180,    "row_position": 1,  "col_position": 4 },
          
          { "element_id": 46,  "rotation": 270,    "row_position": 2,  "col_position": 0 },
          { "element_id": 35,  "rotation": 90,     "row_position": 2,  "col_position": 1 },
          { "element_id": 49,  "rotation": 180,    "row_position": 2,  "col_position": 2 },
          { "element_id": 35,  "rotation": 180,    "row_position": 2,  "col_position": 3 },
          { "element_id": 46,  "rotation": 90,     "row_position": 2,  "col_position": 4 },
          
          { "element_id": 25,  "rotation": 90,     "row_position": 3,  "col_position": 0 },
          { "element_id": 15,  "rotation": 180,    "row_position": 3,  "col_position": 1 },
          { "element_id": 25,  "rotation": 0,      "row_position": 3,  "col_position": 2 },
          { "element_id": 15,  "rotation": 180,    "row_position": 3,  "col_position": 3 },
          { "element_id": 25,  "rotation": 90,     "row_position": 3,  "col_position": 4 },
          
          { "element_id": 25,  "rotation": 270,    "row_position": 4,  "col_position": 0 },
          { "element_id": 35,  "rotation": 180,    "row_position": 4,  "col_position": 1 },
          { "element_id": 25,  "rotation": 270,    "row_position": 4,  "col_position": 2 },
          { "element_id": 25,  "rotation": 90,     "row_position": 4,  "col_position": 3 },
          { "element_id": 35,  "rotation": 180,    "row_position": 4,  "col_position": 4 }
           
        ]},
        { pipe_dimension_id: 3, max_times: 30, max_click: 9, status_id: 2, name: 'Pipe 6', new_pipe_items: [
          {"element_id": 15, "rotation": 270, "row_position": 0, "col_position": 0},
          {"element_id": 25, "rotation": 0,   "row_position": 0, "col_position": 1},
          {"element_id": 46, "rotation": 0,   "row_position": 0, "col_position": 2},
          {"element_id": 25, "rotation": 0,   "row_position": 0, "col_position": 3},
          {"element_id": 49, "rotation": 270, "row_position": 0, "col_position": 4},

          {"element_id": 15, "rotation": 270, "row_position": 1, "col_position": 0},
          {"element_id": 25, "rotation": 0,   "row_position": 1, "col_position": 1},
          {"element_id": 15, "rotation": 180, "row_position": 1, "col_position": 2},
          {"element_id": 25, "rotation": 90,  "row_position": 1, "col_position": 3},
          {"element_id": 35, "rotation": 0,   "row_position": 1, "col_position": 4},

          {"element_id": 35, "rotation": 270, "row_position": 2, "col_position": 0},
          {"element_id": 25, "rotation": 0,   "row_position": 2, "col_position": 1},
          {"element_id": 15, "rotation": 90,  "row_position": 2, "col_position": 2},
          {"element_id": 25, "rotation": 270, "row_position": 2, "col_position": 3},
          {"element_id": 35, "rotation": 0,   "row_position": 2, "col_position": 4},

          {"element_id": 46, "rotation": 270, "row_position": 3, "col_position": 0},
          {"element_id": 15, "rotation": 90,  "row_position": 3, "col_position": 1},
          {"element_id": 15, "rotation": 270, "row_position": 3, "col_position": 2},
          {"element_id": 35, "rotation": 90,  "row_position": 3, "col_position": 3},
          {"element_id": 15, "rotation": 180, "row_position": 3, "col_position": 4},

          {"element_id": 15, "rotation": 270, "row_position": 4, "col_position": 0},
          {"element_id": 15, "rotation": 270, "row_position": 4, "col_position": 1},
          {"element_id": 15, "rotation": 180, "row_position": 4, "col_position": 2},
          {"element_id": 35, "rotation": 180, "row_position": 4, "col_position": 3},
          {"element_id": 15, "rotation": 180, "row_position": 4, "col_position": 4}
          ]}
      ]
  
      conditions_pipe.each do |condition_pipe|
        pipe = Pipe.new(
          pipe_dimension_id: condition_pipe[:pipe_dimension_id],
          max_times: condition_pipe[:max_times],
          max_click: condition_pipe[:max_click],
          name: condition_pipe[:name],
          status_id: condition_pipe[:status_id],
          new_pipe_items: condition_pipe[:new_pipe_items]
        )
  
        if pipe.save
          created_pipes << pipe
        else
          render json: { error: pipe.errors.full_messages }, status: :unprocessable_entity
          return
        end
      end

      pipe_series = []

      created_pipes.each do |created_pipe|
        pipe_ids << created_pipe.id
      end

      conditions_pipe_series = [
        {
          status_id: 1,
          name: 'Pipe Series',
          new_pipe_serie_stages: pipe_ids.map.with_index do |id, index|
            { pipe_id: id, order_no: index + 1 }
          end
        }
      ]

      conditions_pipe_series.each do |condition_pipe_serie|
        pipe_serie = PipeSeries.new(
          name: condition_pipe_serie[:name],
          status_id: condition_pipe_serie[:status_id],
          new_pipe_serie_stages: condition_pipe_serie[:new_pipe_serie_stages]
        )

        if pipe_serie.save
          pipe_series << pipe_serie
        else
          render json: { error: pipe_serie.errors.full_messages }, status: :unprocessable_entity
          return
        end
        render json: pipe_series, status: :created
      end
    end
    
  end 

  def get_none_series
    game_type_id_param = params[:game_type_id]
  
    case game_type_id_param.to_i
    when 1
      working_memories_series_not_in_assignment = WorkingMemorySeries.left_outer_joins(:assignment_items)
        .where(assignment_items: { working_memory_serie_id: nil })
        .order(id: :asc)
  
      if working_memories_series_not_in_assignment.empty?
        render json: { message: 'No orphaned working memory series records found' }, status: :ok
      else
        render json: working_memories_series_not_in_assignment, status: :ok
      end
    when 2
      pipe_series_not_in_assignment = PipeSeries.left_outer_joins(:assignment_item)
        .where(assignment_item: { pipe_serie_id: nil })
        .order(id: :asc)
  
      if pipe_series_not_in_assignment.empty?
        render json: { message: 'No orphaned pipe series records found' }, status: :ok
      else
        render json: pipe_series_not_in_assignment, status: :ok
      end
    else
      render json: { error: 'Invalid game_type_id' }, status: :bad_request
    end
  end

  private

  def set_working_memory_series
    @working_memory_series = WorkingMemorySeries.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Working Memory Series not found' }, status: 404
    return
  end

  def set_pipe_series
    @pipe_series = PipeSeries.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render json: { error: 'Pipe Series not found' }, status: 404
    return
  end

  def working_memory_series_params
    params.require(:working_memory_series).permit(:name, :status_id, :game_type_id, new_working_memory_serie_stages: [:working_memory_id, :order_no])
  end
  
  def pipe_series_params
    params.require(:pipe_series).permit(:name, :status_id, :game_type_id, new_pipe_serie_stages: [:pipe_id, :order_no])
  end

end
