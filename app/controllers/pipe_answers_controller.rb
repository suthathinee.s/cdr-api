class PipeAnswersController < ApplicationController
  before_action :set_pipe_answer, only: %i[ show update destroy ]

  # GET /pipe_answers
  def index
    @pipe_answers = PipeAnswer.search params

    render json: @pipe_answers
  end

  # GET /pipe_answers/1
  def show
    render json: @pipe_answer
  end

  # POST /pipe_answers
  def create
    assignment = Assignment.find_by(id: pipe_answer_params[:assignment_id])
    if assignment.blank?
      render json: { error: 'Assignment not found' }, status: :not_found
      return
    end

    assignment_item = AssignmentItem.find_by(id: pipe_answer_params[:assignment_item_id])
    if assignment_item.blank?
      render json: { error: 'Assignment Item not found' }, status: :not_found
      return
    end

    @pipe_answer = PipeAnswer.new(pipe_answer_params)
    @pipe_answer.start_date = DateTime.current # Set start_date to the current datetime

    if @pipe_answer.save
      render json: @pipe_answer, status: :created, location: @pipe_answer
    else
      render json: @pipe_answer.errors, status: :unprocessable_entity
    end
  end


  # PATCH/PUT /pipe_answers/1
  def update
    assignment = Assignment.find_by(id: pipe_answer_params[:assignment_id])
    if assignment.blank?
      render json: { error: 'Assignment not found' }, status: :not_found
      return
    end

    assignment_item = AssignmentItem.find_by(id: pipe_answer_params[:assignment_item_id])
    if assignment_item.blank?
      render json: { error: 'Assignment Item not found' }, status: :not_found
      return
    end

    if @pipe_answer.update(pipe_answer_params)
      render json: @pipe_answer
    else
      render json: @pipe_answer.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /pipe_answers/1
  def destroy
    @pipe_answer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pipe_answer
      @pipe_answer = PipeAnswer.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Pipe Answer not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    # def pipe_answer_params
    #   params.require(:pipe_answer).permit(:assignment_id, :assignment_item_id, :click_amount, :spent_time, :limit_time ,new_pipe_answer_items: [:answer_obj, :pipe_id, :click_amount, :spent_time, :limit_time, :answer_type_id])
    # end
    def pipe_answer_params
      params.require(:pipe_answer).permit(
        :assignment_id,
        :assignment_item_id,
        :click_amount,
        :spent_time,
        :limit_time,
        :start_date,
        :end_date,
        new_pipe_answer_items: [
          :pipe_id,
          :click_amount,
          :spent_time,
          :limit_time,
          :answer_type_id,
          answer_obj: [
            :pipe_id,
            :rotation,
            { position: [] }  # Use { position: [] } to permit an array
          ]
        ]
      )
    end
    
end
