class AssignmentItemsController < ApplicationController
  before_action :set_assignment_item, only: %i[ show update destroy ]

  # GET /assignment_items
  def index
    @assignment_items = AssignmentItem.search params

    render json: @assignment_items
  end

  # GET /assignment_items/1
  def show
    render json: @assignment_item
  end

  # POST /assignment_items
  def create
    assignment = Assignment.find_by(id: assignment_item_params[:assignment_id])
    if assignment.blank?
      render json: { error: 'Assignment not found' }, status: :not_found
      return
    end

    game_type = GameType.find_by(id: assignment_item_params[:game_type_id])
    if game_type.blank?
      render json: { error: 'Game Type not found' }, status: :not_found
      return
    end

    working_memory_serie = WorkingMemorySerie.find_by(id: assignment_item_params[:working_memory_serie_id])
    if working_memory_serie.blank?
      render json: { error: 'Working Memory Serie not found' }, status: :not_found
      return
    end

    pipe_serie = PipeSerie.find_by(id: assignment_item_params[:pipe_serie_id])
    if pipe_serie.blank?
      render json: { error: 'Pipe Serie not found' }, status: :not_found
      return
    end

    @assignment_item = AssignmentItem.new(assignment_item_params)

    if @assignment_item.save
      render json: @assignment_item, status: :created, location: @assignment_item
    else
      render json: @assignment_item.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /assignment_items/1
  def update
    assignment = Assignment.find_by(id: assignment_item_params[:assignment_id])
    if assignment.blank?
      render json: { error: 'Assignment not found' }, status: :not_found
      return
    end

    game_type = GameType.find_by(id: assignment_item_params[:game_type_id])
    if game_type.blank?
      render json: { error: 'Game Type not found' }, status: :not_found
      return
    end

    working_memory_serie = WorkingMemorySerie.find_by(id: assignment_item_params[:working_memory_serie_id])
    if working_memory_serie.blank?
      render json: { error: 'Working Memory Serie not found' }, status: :not_found
      return
    end

    pipe_serie = PipeSerie.find_by(id: assignment_item_params[:pipe_serie_id])
    if pipe_serie.blank?
      render json: { error: 'Pipe Serie not found' }, status: :not_found
      return
    end
    
    if @assignment_item.update(assignment_item_params)
      render json: @assignment_item
    else
      render json: @assignment_item.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /assignment_items/1
  def destroy
    @assignment_item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assignment_item
      @assignment_item = AssignmentItem.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Assignment Item not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def assignment_item_params
      params.require(:assignment_item).permit(:assignment_id, :order_no, :game_type_id, :working_memory_serie_id, :pipe_serie_id)
    end
    
end
