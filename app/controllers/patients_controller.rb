class PatientsController < ApplicationController
  before_action :set_patient, only: %i[ show update destroy ]

  # GET /patients
  def index
    @patients = Patient.search params

    render json: @patients
  end

  # GET /patients/1
  def show
    render json: @patient
  end

  # POST /patients
  def create
    career = Career.find_by(id: patient_params[:career_id])
    if career.blank?
      render json: { error: 'Career not found' }, status: :not_found
      return
    end

    gender = Gender.find_by(id: patient_params[:gender_id])
    if gender.blank?
      render json: { error: 'Gender not found' }, status: :not_found
      return
    end 

    # tel = patient_params[:tel]
    # if !valid_phone_number?(tel)
    #   render json: { error: 'Tel must be a valid phone number with up to 10 digits' }, status: :unprocessable_entity
    #   return
    # end

    year_of_education = patient_params[:year_of_education]
    if !valid_years_of_education?(year_of_education)
      render json: { error: 'Years of education must be a valid date' }, status: :unprocessable_entity
      return
    end

    @patient = Patient.new(patient_params)
    @patient.first_name = Patient.encode_name(patient_params[:first_name]) # Encode the names
    @patient.last_name = Patient.encode_name(patient_params[:last_name]) # Encode the names

    if @patient.save
      render json: @patient, status: :created, location: @patient
    else
      render json: @patient.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /patients/1
  def update
    career = Career.find_by(id: patient_params[:career_id])
    if career.blank?
      render json: { error: 'Career not found' }, status: :not_found
      return
    end

    gender = Gender.find_by(id: patient_params[:gender_id])
    if gender.blank?
      render json: { error: 'Gender not found' }, status: :not_found
      return
    end

    # tel = patient_params[:tel]
    # if !valid_phone_number?(tel)
    #   render json: { error: 'Tel must be a valid phone number with up to 10 digits' }, status: :unprocessable_entity
    #   return
    # end

    year_of_education = patient_params[:year_of_education]
    if !valid_years_of_education?(year_of_education)
      render json: { error: 'Years of education must be a valid date' }, status: :unprocessable_entity
      return
    end

    if @patient.update(patient_update_params)
  
      render json: @patient
    else
      render json: @patient.errors, status: :unprocessable_entity
      return
    end
  end

  # Helper method to encode a patients
  def encode_patient(patient)
    patient.encode_name # Encode the names
    patient
  end

  # DELETE /patients/1
  def destroy
    @patient.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Patient not found' }, status: 404
      return
    end

    # Helper method to decode the patients's names
    def decode_patients(patient)
      patient.decode_name # Decode the names
      patient
    end

    # Only allow a list of trusted parameters through.
    def patient_params
      params.require(:patient).permit(:first_name, :last_name, :tel, :birthday, :year_of_education, :career_id, :gender_id)
    end

    def patient_update_params
      params[:patient][:first_name] = Doctor.encode_name(params[:patient][:first_name]) # Encode the names
      params[:patient][:last_name] = Doctor.encode_name(params[:patient][:last_name]) # Encode the names

      params.require(:patient).permit(:first_name, :last_name, :tel, :birthday, :year_of_education, :career_id, :gender_id)
    end

    def valid_phone_number?(tel)
      phone_number_pattern = /\A\d{10}\z/
      !tel.nil? && tel.to_s.match?(phone_number_pattern)
    end

    def valid_years_of_education?(years)
      begin
        Date.parse(years)
        return true
      rescue ArgumentError
        return false
      end
    end
end
