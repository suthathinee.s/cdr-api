class ApplicationController < ActionController::API
  prepend_before_action :add_version_header, :bypass_options_method
  before_action :authenticate

  # def authenticate
  #   p "Authentication token"
  #   return render json: { errors: 'Authentication fail.' } #, status: 401 if request.headers['Authorization'].nil?
  #     token = request.headers['Authorization'].split(' ').last
  #   result =  RedisController.get_redis(key: token)
  #   p result
  #   if result < 0
  #     render json: {errors: 'Token is exprie'} #, status: 401
  #   else
  #     decode = ExternalCall.decode(token)
  #     params["IN"] = decode[:shop_code]
      
  #   end
  # end

  def authenticate
    puts "--------------------------------------------------------------------------------------------------------------"
    puts "Authentication token"
    # Check if there's an Authorization header
    if request.headers['Authorization'].nil?
      puts "No Authorization token provided"
      return { errors: 'Authentication fail.' }
    end
  
    token = request.headers['Authorization'].split(' ').last
    result = RedisController.get_redis(key: token)
    puts result
  
    begin
      decoded_token = JWT.decode(token, Login::SECRET_KEY, true, algorithm: 'HS256')
  
      # Access the payload, which includes the claims (such as 'expire_date')
      payload = decoded_token.first
  
      # Access the 'expire_date' claim
      expire_date = Time.at(payload['expire_date'].to_i)
      puts "Token expires at: #{expire_date}"
  
      if result.to_i < 0
        puts "Token is expired"
        return { errors: 'Token is expired' }
      else
        # get session
        username = payload['username']['username']
        find_by_session = User.find_by(username: username)
  
        if find_by_session.present?
          doctor_obj = Doctor.find_by(id: find_by_session.doctor_id)
  
          if doctor_obj.present?
            params["doctor_obj"] = doctor_obj
            params["session"] = {
              username: username,
              role_id: find_by_session.role_permission_id,
              doctor_id: find_by_session.doctor_id,
              doctor_obj: doctor_obj
            }
          else
            params["session"] = {
              username: username,
              role_id: find_by_session.role_permission_id,
              doctor_id: nil,
              doctor_obj: nil
            }
          end
        else
          puts "Token is invalid"
          return { errors: 'Token is invalid' }
        end
      end
  
    rescue JWT::DecodeError => e
      puts "JWT Decode Error: #{e.message}"
      return { errors: 'Invalid token format' }
    end
  
    puts "--------------------------------------------------------------------------------------------------------------"
  end  

  private

  def bypass_options_method
    if request.request_method.upcase == 'OPTIONS'
      head 200
      return false
    end
  end

  def add_version_header
    response.set_header('X-API-Version', '===API_BUILD_VERSION===')
    response.set_header('Access-Control-Expose-Headers', 'X-API-Version, X-Web-Version')
  end
end
