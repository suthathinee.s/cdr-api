class PipeSerieStagesController < ApplicationController
  before_action :set_pipe_serie_stage, only: %i[ show update destroy ]

  # GET /pipe_serie_stages
  def index
    @pipe_serie_stages = PipeSerieStage.search params

    render json: @pipe_serie_stages
  end

  # GET /pipe_serie_stages/1
  def show
    render json: @pipe_serie_stage
  end

  # POST /pipe_serie_stages
  def create
    pipe_serie = PipeSerie.find_by(id: pipe_serie_stage_params[:pipe_serie_id])
    if pipe_serie.blank?
      render json: { error: 'Pipe Serie not found' }, status: :not_found
      return
    end

    @pipe_serie_stage = PipeSerieStage.new(pipe_serie_stage_params)

    if @pipe_serie_stage.save
      render json: @pipe_serie_stage, status: :created, location: @pipe_serie_stage
    else
      render json: @pipe_serie_stage.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /pipe_serie_stages/1
  def update
    pipe_serie = PipeSerie.find_by(id: pipe_serie_stage_params[:pipe_serie_id])
    if pipe_serie.blank?
      render json: { error: 'Pipe Serie not found' }, status: :not_found
      return
    end
    
    if @pipe_serie_stage.update(pipe_serie_stage_params)
      render json: @pipe_serie_stage
    else
      render json: @pipe_serie_stage.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /pipe_serie_stages/1
  def destroy
    @pipe_serie_stage.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pipe_serie_stage
      @pipe_serie_stage = PipeSerieStage.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Pipe Answer not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def pipe_serie_stage_params
      params.require(:pipe_serie_stage).permit(:pipe_serie_id, :pipe_id, :order_no)
    end
end
