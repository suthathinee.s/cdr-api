class GendersController < ApplicationController
  before_action :set_gender, only: %i[ show update destroy ]

  # GET /genders
  def index
    @genders = Gender.search params

    render json: @genders
  end

  # GET /genders/1
  def show
    render json: @gender
  end

  # POST /genders
  def create
    @gender = Gender.new(gender_params)

    if @gender.save
      render json: @gender, status: :created, location: @gender
    else
      render json: @gender.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /genders/1
  def update
    if @gender.update(gender_params)
      render json: @gender
    else
      render json: @gender.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /genders/1
  def destroy
    @gender.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gender
      @gender = Gender.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Gender not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def gender_params
      params.require(:gender).permit(:name)
    end
end
