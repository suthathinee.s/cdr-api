class ElementTypesController < ApplicationController
  before_action :set_element_type, only: %i[ show update destroy ]

  # GET /element_types
  def index
    @element_types = ElementType.search params

    render json: @element_types
  end

  # GET /element_types/1
  def show
    render json: @element_type
  end

  # POST /element_types
  def create
    if element_type_params[:name].blank?
      render json: { error: 'Element Type name not found' }, status: 400
      return
    end

    @element_type = ElementType.new(element_type_params)

    if @element_type.save
      render json: @element_type, status: :created, location: @element_type
    else
      render json: @element_type.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /element_types/1
  def update
    if element_type_params[:name].blank?
      render json: { error: 'Element Type name not found' }, status: 400
      return
    end
    
    if @element_type.update(element_type_params)
      render json: @element_type
    else
      render json: @element_type.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /element_types/1
  def destroy
    @element_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_element_type
      @element_type = ElementType.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Element Type not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def element_type_params
      params.require(:element_type).permit(:name)
    end
end
