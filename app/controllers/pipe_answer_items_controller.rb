class PipeAnswerItemsController < ApplicationController
  before_action :set_pipe_answer_item, only: %i[ show update destroy ]

  # GET /pipe_answer_items
  def index
    @pipe_answer_items = PipeAnswerItem.search params

    render json: @pipe_answer_items
  end

  # GET /pipe_answer_items/1
  def show
    render json: @pipe_answer_item
  end

  # POST /pipe_answer_items
  def create
    pipe_answer = PipeAnswer.find_by(id: pipe_answer_item_params[:pipe_answer_id])
    if pipe_answer.blank?
      render json: { error: 'Pipe Answer not found' }, status: :not_found
      return
    end

    pipe = Pipe.find_by(id: pipe_answer_item_params[:pipe_id])
    if pipe.blank?
      render json: { error: 'Pipe not found' }, status: :not_found
      return
    end

    answer_type = AnswerType.find_by(id: pipe_answer_item_params[:answer_type_id])
    if answer_type.blank?
      render json: { error: 'Answer Type not found' }, status: :not_found
      return
    end

    @pipe_answer_item = PipeAnswerItem.new(pipe_answer_item_params)

    if @pipe_answer_item.save
      render json: @pipe_answer_item, status: :created, location: @pipe_answer_item
    else
      render json: @pipe_answer_item.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /pipe_answer_items/1
  def update
    pipe_answer = PipeAnswer.find_by(id: pipe_answer_item_params[:pipe_answer_id])
    if pipe_answer.blank?
      render json: { error: 'Pipe Answer not found' }, status: :not_found
      return
    end

    pipe = Pipe.find_by(id: pipe_answer_item_params[:pipe_id])
    if pipe.blank?
      render json: { error: 'Pipe not found' }, status: :not_found
      return
    end

    answer_type = AnswerType.find_by(id: pipe_answer_item_params[:answer_type_id])
    if answer_type.blank?
      render json: { error: 'Answer Type not found' }, status: :not_found
      return
    end
    
    if @pipe_answer_item.update(pipe_answer_item_params)
      render json: @pipe_answer_item
    else
      render json: @pipe_answer_item.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /pipe_answer_items/1
  def destroy
    @pipe_answer_item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pipe_answer_item
      @pipe_answer_item = PipeAnswerItem.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Pipe Answer Item not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def pipe_answer_item_params
      params.require(:pipe_answer_item).permit(:pipe_answer_id, :answer_obj, :pipe_id, :click_amount, :spent_time, :limit_time, :answer_type_id)
    end
end
