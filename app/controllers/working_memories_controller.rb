class WorkingMemoriesController < ApplicationController
  before_action :set_working_memory, only: %i[ show update destroy ]

  # GET /working_memories/none-using
  def none_using
    working_memories_not_in_series = WorkingMemory.left_outer_joins(:working_memory_serie_stages)
      .where(working_memory_serie_stages: { id: nil })
      .order(id: :asc)  # Sort by id in ascending order

    if working_memories_not_in_series.empty?
      render json: { message: 'No orphaned working memory records found' }, status: :ok
    else
      render json: working_memories_not_in_series, status: :ok
    end
  end
  
  # GET /working_memories
  def index
    @working_memories = WorkingMemory.search params

    render json: @working_memories
  end

  # GET /working_memories/1
  def show
    render json: @working_memory
  end

  # POST /working_memories
  def create
    working_memory_size = WorkingMemorySize.find_by(id: working_memory_params[:working_memory_size_id])
    if working_memory_size.blank?
      render json: { error: 'Working Memory Size not found' }, status: :not_found
      return
    end
    
    max_times = working_memory_params[:max_times]
    if max_times.blank?
      render json: { error: 'Max Times not found' }, status: :not_found
      return
    end
    if !max_times.is_a?(Integer)
      render json: { error: 'Max Times must be an integer' }, status: :unprocessable_entity
      return
    end    

    status = Status.find_by(id: working_memory_params[:status_id])
    if status.blank?
      render json: { error: 'Status not found' }, status: :not_found
      return
    end

    if working_memory_params[:new_working_memory_items].count != working_memory_size.size
      render json: { error: "Working Memory Items must have exactly #{working_memory_size.size} items" }, status: :unprocessable_entity
      return
    end

    @working_memory = WorkingMemory.new(working_memory_params)
    if @working_memory.save
      render json: @working_memory, status: :created, location: @working_memory
    else
      render json: @working_memory.errors, status: :unprocessable_entity
      return
    end
  end

  # PATCH/PUT /working_memories/1
  def update
    working_memory_size = WorkingMemorySize.find_by(id: working_memory_params[:working_memory_size_id])
    if working_memory_size.blank?
      render json: { error: 'Working Memory Size not found' }, status: :not_found
      return
    end

    max_times = working_memory_params[:max_times]
    if max_times.blank?
      render json: { error: 'Max Times not found' }, status: :not_found
      return
    end
    if !max_times.is_a?(Integer)
      render json: { error: 'Max Times must be an integer' }, status: :unprocessable_entity
      return
    end  

    status = Status.find_by(id: working_memory_params[:status_id])
    if status.blank?
      render json: { error: 'Status not found' }, status: :not_found
      return
    end

    if @working_memory.update(working_memory_params)
      render json: @working_memory
    else
      render json: @working_memory.errors, status: :unprocessable_entity
      return
    end
  end

  # DELETE /working_memories/1
  def destroy
    @working_memory.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_working_memory
      @working_memory = WorkingMemory.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Working Memory not found' }, status: 404
      return
    end

    # Only allow a list of trusted parameters through.
    def working_memory_params
      params.require(:working_memory).permit(:working_memory_size_id, :max_times, :name, :status_id, new_working_memory_items: [:element_id ,:position])
    end
end
