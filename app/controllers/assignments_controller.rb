class AssignmentsController < ApplicationController
  before_action :set_assignment, only: %i[ show update destroy ]

  # GET /assignments
  def index
    @assignments = Assignment.search params

    
    render json: @assignments
  end

  # GET /assignments/1
  def show
    if @assignment
      if @assignment.id == 6
        # Set start_date and end_date to nil for all assignments
        Assignment.update_all(start_date: nil, end_date: nil)
        WorkingMemoryAnswer.destroy_all
        PipeAnswer.destroy_all
      end

      render json: @assignment
    else
      render json: { error: 'Assignment not found' }, status: :not_found
    end
  end

  # POST /assignments
  def create
    patient = Patient.find_by(id: assignment_params[:patient_id])
    if patient.blank?
      render json: { error: 'Patient not found' }, status: :not_found
      return
    end

    doctor = Doctor.find_by(id: assignment_params[:doctor_id])
    if doctor.blank?
      render json: { error: 'Doctor not found' }, status: :not_found
      return
    end
    
    @assignment = Assignment.new(assignment_params)
    @assignment.code = Assignment.generate_random_string(7)
    
    if @assignment.save
      render json: @assignment, status: :created, location: @assignment
    else
      render json: @assignment.errors, status: :unprocessable_entity
      return
    end
  end

   # PATCH/PUT /assignments/1
  def update
    if @assignment
      if assignment_params[:start_date].present?
        if @assignment.start_date == nil
        elsif @assignment.end_date == nil && @assignment.start_date != nil
          working_memory_answer_id = WorkingMemoryAnswer.find_by(assignment_id: @assignment.id)
          pipe_answer_id = PipeAnswer.find_by(assignment_id: @assignment.id)

          if working_memory_answer_id.present?
            WorkingMemoryAnswer.where(assignment_id: @assignment.id).destroy_all
          end

          if pipe_answer_id.present?
            PipeAnswer.where(assignment_id: @assignment.id).destroy_all
          end
        else
          render json: { error: 'Assignment already done' }, status: :unprocessable_entity
          return
        end
      else
        if assignment_params[:end_date].present?
          if @assignment.end_date != nil
            render json: { error: 'Assignment already done' }, status: :unprocessable_entity
            return
          end
        else
          render json: { error: 'Assignment not found' }, status: :not_found
          return
        end
      end

      if @assignment.update(assignment_params)
        render json: @assignment
      else
        render json: @assignment.errors, status: :unprocessable_entity
        return
      end      
    else
      render json: { error: 'Assignment not found' }, status: :not_found
    end
  end

  # DELETE /assignments/1
  def destroy
    if @assignment
      @assignment.destroy
      render json: { message: 'Assignment deleted successfully' }
    else
      render json: { error: 'Assignment not found' }, status: :not_found
    end
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assignment
        if params[:code].present?
          @assignment = Assignment.find_by(code: params[:code])
          if @assignment.blank?
            render json: { error: 'Assignment not found' }, status: 404
            return
          end
        end
        @assignment = Assignment.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'Assignment not found' }, status: 404
      return
    end
    
    # Only allow a list of trusted parameters through.
    def assignment_params
      params.require(:assignment).permit(:patient_id, :doctor_id, :start_date, :end_date, new_assignment_items: [:order_no, :game_type_id, :working_memory_serie_id, :pipe_serie_id])
    end
end
