class RedisController < ApplicationController
  require 'redis'
  
  @Redis_conn = Redis.new(host: "localhost", port: 6379, db: 0)

  def self.set_redis(params = {})
    key = params[:key]
    value = params[:value]
    @Redis_conn.set(key, value, ex: 21600)
  end

  def self.get_redis(params = {})
    key = params[:key]
    @Redis_conn.get(key)
    remaining_time = @Redis_conn.ttl(key)
  end

  def self.del_redis(key)
    @Redis_conn.del(key)
  end
end
