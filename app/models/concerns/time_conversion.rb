module TimeConversion
  def seconds_to_hms(seconds)
    hours = seconds / 3600
    seconds %= 3600
    minutes = seconds / 60
    seconds %= 60
    "#{hours} hours, #{minutes} minutes, #{seconds} seconds"
  end
end