class WorkingMemoryAnswer < ApplicationRecord
  belongs_to :assignment
  belongs_to :assignment_item
  has_many :working_memory_answer_items

  attr_accessor :new_working_memory_answer_items

  after_save :save_working_memory_answer_items

  validate :validate_assignment_item

  def validate_assignment_item
    if assignment_item
      if assignment_item.game_type&.name != "Working memory" || assignment_item.assignment_id != assignment_id
        errors.add(:assignment_item, "is not valid for creating a Working memory Answer")
        return
      end
    end
  end

  def as_json(options = {})
    super().merge!(
      assignment_name: assignment&.name,
      patient_full_name: Patient.decode_name(assignment&.patient&.first_name.to_s) + " " + Patient.decode_name(assignment&.patient&.last_name.to_s),
      game_type_id: assignment_item&.game_type&.id,
      game_type: assignment_item&.game_type&.name,
      working_memory_answer_items: working_memory_answer_items.map do |working_memory_answer_item_table|
        get_wm = WorkingMemory.find_by(id: working_memory_answer_item_table.working_memory_id)
  
        answers_array = []
  
        working_memory_answer_item_table.answer.each do |key, value|
          key_as_integer = key.to_i
          get_wmi = WorkingMemoryItem.find_by(working_memory_id: get_wm.id, position: key_as_integer)
          element_id = get_wmi&.element&.id.to_i
          element = Element.find_by(id: element_id)
  
          patient_answer =
            case value.downcase
            when "triangle" then 10
            when "square" then 11
            when "pentagon" then 12
            when "octagon" then 13
            when "hexagon" then 14
            else
              value.to_i
            end
  
          patient_answer_element = Element.find_by(id: patient_answer)
  
          answers_array << {
            "question": key_as_integer,
            "answer_element_id" => patient_answer,
            "is_correct" => (patient_answer == element_id) ? true : false,
            "answer_file_url" => patient_answer_element&.image_url
          }
        end
  
        {
          working_memory_answer_item_id: working_memory_answer_item_table.id,
          working_memory_id: get_wm&.id,
          answers: answers_array,
          working_memory_answer_id: working_memory_answer_item_table.working_memory_answer_id,
          answer_correct: working_memory_answer_item_table.answer_correct,
          answer_wrong: working_memory_answer_item_table.answer_wrong
        }
      end
    )
  end
  

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:name]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end

  def save_working_memory_answer_items
    p "--------------------#{new_working_memory_answer_items}-------------------"
    p "--------------------#{self}-------------------"
    if new_working_memory_answer_items
      item_amount = WorkingMemoryAnswerItem.where(working_memory_answer_id: self.id).count

      if item_amount < 14
        new_obj_ids = []
          new_working_memory_answer_items.each do |attr|
            obj = attr[:id].blank? ? working_memory_answer_items.build : WorkingMemorySerieStage.find(attr[:id])
            p "--------------------#{obj}-------------------"
            obj.attributes = attr
            obj.save
            new_obj_ids << obj.id
          end
          #shop_group_shops :  call use relations shop_group_shops and shop_groups
          #find old Items and remove id not in new_obj_ids
          # working_memory_answer_items.each { |e| e.destroy unless new_obj_ids.include?(e.id) }
          reload
        end
      else
        errors.add(:new_working_memory_answer_items, "can have at most 14 items")
        return
      end
  end
end
