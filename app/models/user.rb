class User < ApplicationRecord
  include Rails.application.routes.url_helpers
  belongs_to :doctor
  belongs_to :role_permission

  attr_accessor :password_confirmation 
  validates :username, uniqueness: true
  #validates :password
  validates :password ,length: {in: 4..40} , if: :password_required?
  # validates :password ,:password_confirmation ,presence: true ,if: :password_required?
  validates :password, confirmation: true, if: :password_required?

  before_save :encrypt_password

  def as_json(options = {})
    super().merge(
    )
  end
  

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = []
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end

  #call anather class
  def self.encrypt(p)
    salt = "==#CDR-API-Salt-User#=="
    Digest::SHA512.hexdigest("=#{salt}#{p}#{salt}=")
  end

  #call in call
  def encrypt(p)
    self.class.encrypt(p)
  end

  #encrypt password and update or crate record in UserCrypted
  def encrypt_password
    return if password.blank?
    self.password =  encrypt(password)

  end

  # password_required?
  def password_required?
    # crypted_password.blank? || !password.blank? || !password_confirmation.blank?
    !password.blank? || !password_confirmation.blank?
  end

end