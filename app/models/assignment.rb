class Assignment < ApplicationRecord
  has_many :assignment_items
  belongs_to :patient
  belongs_to :doctor
  has_many :working_memory_answers
  has_many :pipe_answers

  attr_accessor :new_assignment_items

  after_save :save_assignment_items

  validate :validate_game_type_and_series

  def self.generate_random_string(length)
    characters = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    string = (0...length).map { characters[rand(characters.length)] }.join
    number = SecureRandom.random_number(10**1).to_s.rjust(1, '0')
    return string + number
  end

  def as_json(options = {})
    super().merge!(
      # other attributes
      patient_full_name: Patient.decode_name(patient.first_name.to_s) + " " + Patient.decode_name(patient&.last_name),
      doctor_full_name: Doctor.decode_name(doctor.first_name.to_s) + " " + Doctor.decode_name(doctor&.last_name),
      patient_career: patient&.career&.name,
      patient_age: patient&.calulate_age(patient&.birthday),
      patient_year_of_education: patient&.year_of_education,
      working_memory_series_name: assignment_items.map { |item| item.working_memory_serie&.name }.compact.first,
      pipe_series_name: assignment_items.map { |item| item.pipe_serie&.name }.compact.first,
      assignment_items: assignment_items.map do |assignment_item_table|
        if assignment_item_table.working_memory_serie.present?
          working_memory_serie = {
            id: assignment_item_table.working_memory_serie.id,
            name: assignment_item_table.working_memory_serie.name,
            status: assignment_item_table.working_memory_serie.status
          }
        else
          working_memory_serie = nil
        end

        if assignment_item_table.pipe_serie.present?
          pipe_serie = {
            id: assignment_item_table.pipe_serie.id,
            name: assignment_item_table.pipe_serie.name,
            status: assignment_item_table.pipe_serie.status
          }
        else
          pipe_serie = nil
        end
  
        {
          # patient: Patient.decode_name(patient.first_name.to_s) + " " + Patient.decode_name(patient&.last_name),
          # doctor: Doctor.decode_name(doctor.first_name.to_s) + " " + Doctor.decode_name(doctor&.last_name),   
          assignment_item_id: assignment_item_table.id, 
          order_no: assignment_item_table.order_no,
          game_type: assignment_item_table.game_type&.name,
          working_memory_serie: working_memory_serie,
          pipe_serie: pipe_serie
        }
      end
    )
  end

  def validate_game_type_and_series
    if new_assignment_items.present?
      new_assignment_items.each do |item|
        if item['game_type_id'] == 1 && item['pipe_serie_id'].present?
          errors.add(:base, "Pipe serie must be null when game type is 1")
          return
        elsif item['game_type_id'] == 2 && item['working_memory_serie_id'].present?
          errors.add(:base, "Working memory serie must be null when game type is 2")
          return
        end
      end
    end
  end

  def self.search(params = {})
    data = all
  
    data = data.select %(
      #{table_name}.*,
      #{Arel.sql("convert_from(decode(patients.first_name, 'base64'), 'UTF-8')")} AS patient_first_name,
      #{Arel.sql("convert_from(decode(patients.last_name, 'base64'), 'UTF-8')")} AS patient_last_name,
      #{Arel.sql("convert_from(decode(doctors.first_name, 'base64'), 'UTF-8')")} AS doctor_first_name,
      #{Arel.sql("convert_from(decode(doctors.last_name, 'base64'), 'UTF-8')")} AS doctor_last_name,
      patients.first_name || ' ' || patients.last_name AS patient_full_name,
      doctors.first_name || ' ' || doctors.last_name AS doctor_full_name
    )
  
    data = data.joins("LEFT JOIN doctors ON doctors.id = #{table_name}.doctor_id")
               .joins("LEFT JOIN patients ON patients.id = #{table_name}.patient_id")
  
    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:code, :"patients.first_name || ' ' || patients.last_name", :"doctors.first_name || ' ' || doctors.last_name"]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
  
    if params[:order] == "patient_full_name asc"
      params[:order] = Arel.sql("convert_from(decode(patients.first_name, 'base64'), 'UTF-8'), convert_from(decode(patients.last_name, 'base64'), 'UTF-8')")
    elsif params[:order] == "patient_full_name desc"
      params[:order] = Arel.sql("convert_from(decode(patients.first_name, 'base64'), 'UTF-8') desc, convert_from(decode(patients.last_name, 'base64'), 'UTF-8') desc")
    elsif params[:order] == "doctor_full_name asc"
      params[:order] = Arel.sql("convert_from(decode(doctors.first_name, 'base64'), 'UTF-8'), convert_from(decode(doctors.last_name, 'base64'), 'UTF-8')")
    elsif params[:order] == "doctor_full_name desc"
      params[:order] = Arel.sql("convert_from(decode(doctors.first_name, 'base64'), 'UTF-8') desc, convert_from(decode(doctors.last_name, 'base64'), 'UTF-8') desc")
    end
  
    super(params.merge!(data: data))
  end
  
  
  def save_assignment_items
    p "--------------------#{new_assignment_items}-------------------"
    p "--------------------#{self}-------------------"
    if new_assignment_items
      new_obj_ids = []
      new_assignment_items.each do |attr|
        obj = attr[:id].blank? ? assignment_items.build : AssignmentItem.find(attr[:id])
        p "--------------------#{obj}-------------------"
        obj.attributes = attr
        obj.save
        new_obj_ids << obj.id
      end
      #shop_group_shops :  call use relations shop_group_shops and shop_groups
      #find old Items and remove id not in new_obj_ids
      assignment_items.each { |e| e.destroy unless new_obj_ids.include?(e.id) }
      reload
    end
  end

end