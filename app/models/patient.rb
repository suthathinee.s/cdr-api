class Patient < ApplicationRecord
  belongs_to :gender
  belongs_to :career
  has_many :assignments

  def calulate_age(birthday)
    if birthday.present?
      now = Time.now.utc.to_date
      return now.year - birthday.year - (birthday.to_date.change(:year => now.year) > now ? 1 : 0)
    else
      return nil # Or handle this case as appropriate for your application
    end
  end

  def as_json(options = {})
    super(except: [:career_id, :gender_id, :created_at, :updated_at]).merge!(
      full_name: Patient.decode_name(self.first_name.to_s) + " " + Patient.decode_name(self.last_name.to_s),
      first_name: Patient.decode_name(self.first_name.to_s),
      last_name: Patient.decode_name(self.last_name.to_s),
      career_id: career&.id,
      career:career&.name,
      gender_id: gender&.id,
      gender: gender&.name,
      Age: calulate_age(self.birthday)
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*,
      patients.first_name || ' ' || patients.last_name as full_name
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:"patients.first_name || ' ' || patients.last_name", :tel]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true

    if params[:order] == "full_name asc"
      params[:order] = Arel.sql("convert_from(decode(patients.first_name, 'base64'), 'UTF-8'), convert_from(decode(patients.last_name, 'base64'), 'UTF-8')")
    elsif params[:order] == "full_name desc"
      params[:order] = Arel.sql("convert_from(decode(patients.first_name, 'base64'), 'UTF-8') desc, convert_from(decode(patients.last_name, 'base64'), 'UTF-8') desc")
    end
    
    super(params.merge!(data: data))
  end

  def self.encode_name(str_name)
    Base64.strict_encode64(str_name).force_encoding("UTF-8")
 end

 # Decode the first name and last name
 def self.decode_name(str_name)
   begin
     decoded_name = Base64.strict_decode64(str_name).force_encoding('UTF-8')
   rescue ArgumentError
     # Handle the error, e.g., return an empty string or raise a custom exception
     decoded_name = '' # You can adjust this behavior as needed
   end
   decoded_name
 end

end
