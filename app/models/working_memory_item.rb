class WorkingMemoryItem < ApplicationRecord
  belongs_to :working_memory
  belongs_to :element

  def as_json(options = {})
    super(except: [:working_memory_id, :element_id, :game_type_id, :created_at, :updated_at]).merge!(
      element: element
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = []
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end
end
