class PipeItem < ApplicationRecord
  belongs_to :pipe
  belongs_to :element

  def as_json(options = {})
    super(except: [:pipe_id, :element_id, :created_at, :updated_at]).merge!(
      element: element
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = []
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end
end
