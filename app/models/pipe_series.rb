class PipeSeries < ApplicationRecord
  belongs_to :status
  belongs_to :game_type
  has_many :assignment_item, foreign_key: 'pipe_serie_id'
  has_many :pipe_serie_stages, dependent: :destroy

  attr_accessor :new_pipe_serie_stages

  after_save :save_pipe_series_stages

  def as_json(options = {})
    super(except: [:status_id, :created_at, :updated_at]).merge!(
      status: status&.name,
      game_type: game_type&.name,
      pipe_serie_stages: pipe_serie_stages
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:name]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end

  def save_pipe_series_stages
    p "--------------------#{new_pipe_serie_stages}-------------------"
    p "--------------------#{self}-------------------"
    if new_pipe_serie_stages
      new_obj_ids = []
      new_pipe_serie_stages.each do |attr|
        obj = if attr.is_a?(Hash) && attr.key?("id") && attr["id"].blank?
          pipe_serie_stages.build
        elsif attr.is_a?(String) && attr.blank?
          pipe_serie_stages.build
        else
          found_obj = PipeSerieStage.find_by(id: attr["id"])
          if found_obj
            found_obj.attributes = attr
            found_obj.save
          else
            pipe_serie_stages.build(attr)
          end
        end
        p "--------------------#{obj}-------------------"
        obj.attributes = attr
        obj.save
        new_obj_ids << obj.id
      end
      #shop_group_shops :  call use relations shop_group_shops and shop_groups
      #find old Items and remove id not in new_obj_ids
      pipe_serie_stages.each { |e| e.destroy unless new_obj_ids.include?(e.id) }
      reload
    end
  end

end
