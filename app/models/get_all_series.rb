class GetAllSeries < ApplicationRecord

  def self.table_all_series(params = {})
    # limit offset
    page = params[:page].nil? ? 1 : params[:page].to_i
    limit = params[:limit].nil? ? 200 : params[:limit].to_i
    offset = (page - 1) * limit if page > 0
    conn = ActiveRecord::Base.connection
  
    name_filter_pipe = params[:keywords].present? ? "AND pipe_series.name ILIKE '%#{params[:keywords]}%'" : ""
    name_filter_working_memory = params[:keywords].present? ? "AND working_memory_series.name ILIKE '%#{params[:keywords]}%'" : ""

    game_type_filter_pipe = params[:game_type_id].present? ? "AND pipe_series.game_type_id IN (#{Array(params[:game_type_id]).join(',')})" : ""
    game_type_filter_working_memory = params[:game_type_id].present? ? "AND working_memory_series.game_type_id IN (#{Array(params[:game_type_id]).join(',')})" : ""
    
    status_filter_pipe = params[:status_id].present? ? "AND pipe_series.status_id IN (#{Array(params[:status_id]).join(',')})" : ""
    status_filter_working_memory = params[:status_id].present? ? "AND working_memory_series.status_id IN (#{Array(params[:status_id]).join(',')})" : ""

    order_by = case params[:order]
              when 'name asc'
                'ORDER BY name ASC'
              when 'name desc'
                'ORDER BY name DESC'
              else
                '' # Default order
              end

    sql = %{
      SELECT 
        pipe_series.id as series_id,
        pipe_series.name as name,
        statuses.id as status_id,
        statuses.name as status,
        game_types.id as game_type_id,
        game_types.name as game_type
      FROM 
        pipe_series
      LEFT JOIN 
        statuses ON pipe_series.status_id = statuses.id
      LEFT JOIN
        game_types ON pipe_series.game_type_id = game_types.id
      WHERE 1=1 #{name_filter_pipe} #{game_type_filter_pipe} #{status_filter_pipe}
      UNION
      
      SELECT 
        working_memory_series.id as series_id,
        working_memory_series.name as name,
        statuses.id as status_id,
        statuses.name as status,
        game_types.id as game_type_id,
        game_types.name as game_type
      FROM 
        working_memory_series
      LEFT JOIN 
        statuses ON working_memory_series.status_id = statuses.id
      LEFT JOIN
        game_types ON working_memory_series.game_type_id = game_types.id
      WHERE 1=1 #{name_filter_working_memory} #{game_type_filter_working_memory} #{status_filter_working_memory}
      #{order_by};
    }
  
    results = conn.execute(sql).to_a
    data = results[offset, limit]
  
    if data.blank?
      {
        series_id: [],
        name: [],
        status_id: [],
        status: [],
        game_type_id: [],
        game_type: []
      }
    else
      data = data.map do |result|
        {
          series_id: result['series_id'],
          name: result['name'],
          status_id: result['status_id'],
          status: result['status'],
          game_type_id: result['game_type_id'],
          game_type: result['game_type']
        }
      end
    end
  
    total_pages = (results.length.to_i / limit.to_i).ceil
  
    if params[:datatable] == 'true'
      return {
        data: data,
        total: results.length,
        total_page: total_pages,
        page: page,
        limit: limit
      }
    else
      data
    end
  end
  

  def self.patients_imformation(params = {})
    
    #limit offset
    page = params[:page].nil? ? 1 : params[:page].to_i
    limit = params[:limit].nil? ? 200 : params[:limit].to_i
    offset =  (page - 1) * limit if page > 0
    conn = ActiveRecord::Base.connection

    order_by = case params[:order]
    when 'patient_first_name asc'
      'ORDER BY patient_first_name ASC'
    when 'patient_first_name desc'
      'ORDER BY patient_first_name DESC'
    else
      'ORDER BY p.id' # Default order
    end

    sql = %{
      SELECT 
        p.id as patient_id,
        avs.total_patients,
        convert_from(decode(p.first_name, 'base64'), 'UTF8') as patient_first_name,
        convert_from(decode(p.last_name, 'base64'), 'UTF8') as patient_last_name,
        ROUND(EXTRACT(YEAR FROM AGE(NOW(), p.birthday))::NUMERIC, 0)::INT as patient_age,
        ROUND(avs.avg_patient_age::NUMERIC, 0)::INT as avg_patient_age,  -- Explicit casting
        ca.total_assignment,
        COALESCE(ccw.working_memory_question, 0) as working_memory_question,
        COALESCE(ccw.total_answer_correct, 0)  as working_memory_total_correct,
        COALESCE(ccw.total_answer_wrong, 0) as working_memory_total_wrong,
        COALESCE(cpf.pipe_question, 0) as pipe_question,
        COALESCE(cpf.pass_count, 0) as pipe_total_pass,
        COALESCE(cpf.fail_count, 0) as pipe_total_fail
      FROM patients p

      -- Joining count_pass_fail
      LEFT JOIN (
        SELECT 
          a.patient_id as patient_id,
          COUNT(DISTINCT pai.id) as pipe_question,
          SUM(CASE WHEN at.name = 'Pass' THEN 1 ELSE 0 END) as pass_count,
          SUM(CASE WHEN at.name = 'Failed' THEN 1 ELSE 0 END) as fail_count
        FROM assignments a
        LEFT JOIN pipe_answers pa ON pa.assignment_id = a.id
        LEFT JOIN pipe_answer_items pai ON pai.pipe_answer_id = pa.id
        LEFT JOIN answer_types at ON at.id = pai.answer_type_id
        GROUP BY a.patient_id
      ) cpf ON cpf.patient_id = p.id

      -- Joining count_correct_wrong
      LEFT JOIN (
        SELECT
        a.patient_id as patient_id,
          COUNT(DISTINCT wmai.id) as working_memory_question,
          SUM(wmai.answer_correct) as total_answer_correct,
          SUM(wmai.answer_wrong) as total_answer_wrong
        FROM assignments a
        LEFT JOIN working_memory_answers wma ON wma.assignment_id = a.id
        LEFT JOIN working_memory_answer_items wmai ON wmai.working_memory_answer_id = wma.id
        GROUP BY a.patient_id
      ) ccw ON ccw.patient_id = p.id

      -- Joining count_assignment
      LEFT JOIN (
        SELECT
          a.patient_id as patient_id,
          COUNT(DISTINCT a.id) as total_assignment
        FROM assignments a
        GROUP BY a.patient_id
      ) ca ON ca.patient_id = p.id

      -- Joining average_summary
      CROSS JOIN (
        SELECT
          COUNT(DISTINCT p.id) as total_patients,
          AVG(EXTRACT(YEAR FROM AGE(NOW(), p.birthday))) as avg_patient_age
        FROM patients p
      ) avs

      GROUP BY
        p.id,
        ccw.working_memory_question,
        ccw.total_answer_correct,
        ccw.total_answer_wrong,
        ca.total_assignment,
        avs.avg_patient_age,
        avs.total_patients,
        cpf.pass_count,
        cpf.fail_count,
        cpf.pipe_question
        #{order_by};
    }
    
    results = conn.execute(sql).to_a
    data = results[offset, limit]
    
    if data.blank?
      {
        patient_id: [],
        total_patients: [],
        patient_first_name: [],
        patient_last_name: [],
        patient_age: [],
        avg_patient_age: [],
        total_assignment: [],
        working_memory_question: [],
        working_memory_total_correct: [],
        working_memory_total_wrong: [],
        pipe_question: [],
        pipe_total_pass: [],
        pipe_total_fail: []
      }
    else
      data = data.map do |results| 
        {
          patient_id: results["patient_id"],
          total_patients: results["total_patients"],
          patient_first_name: results["patient_first_name"],
          patient_last_name: results["patient_last_name"],
          patient_age: results["patient_age"],
          avg_patient_age: results["avg_patient_age"],
          total_assignment: results["total_assignment"],
          working_memory_question: results["working_memory_question"],
          working_memory_total_correct: results["working_memory_total_correct"],
          working_memory_total_wrong: results["working_memory_total_wrong"],
          pipe_question: results["pipe_question"],
          pipe_total_pass: results["pipe_total_pass"],
          pipe_total_fail: results["pipe_total_fail"]
        }
      end    
    end
    
    total_pages = (results.length.to_i / limit.to_i).ceil
    
    if params[:datatable] == 'true'  # Fixed equality check
      return {
        data: data,
        total: results.length,
        total_page: total_pages,
        page: page,
        limit: limit
      }
    else
      data
    end
	end

  def self.working_memory_answer_report(params = {})

    # limit offset
    page = params[:page].nil? ? 1 : params[:page].to_i
    limit = params[:limit].nil? ? 200 : params[:limit].to_i
    offset = (page - 1) * limit if page > 0
    conn = ActiveRecord::Base.connection

    sql = %{
      SELECT
        wma.id as id,
        a.id as assignment_id,
        wma.assignment_item_id as assignment_item_id,
        wma.answer_correct as answer_correct,
        wma.answer_wrong as answer_wrong,
        wma.start_date as start_date,
        wma.end_date as end_date,
        a.name as assignment_name,
        jsonb_agg(jsonb_build_object(
          'working_memory_answer_item_id', wmai.id,
          'working_memory_id', wmai.working_memory_id,
          'working_memory_answer_id', wmai.working_memory_answer_id,
          'answers', (
            SELECT jsonb_agg(jsonb_build_object(
              'question', (key::int),
              'answer_element_id', 
              CASE 
                WHEN value = 'triangle' THEN 10
                WHEN value = 'square' THEN 11
                WHEN value = 'pentagon' THEN 12
                WHEN value = 'octagon' THEN 13
                WHEN value = 'hexagon' THEN 14
                ELSE (value::int)
              END,
              'is_correct', null,
              'working_memory_id', wmai.working_memory_id,
              'answer_file_url', null
            ))
            FROM jsonb_each_text(wmai.answer::jsonb)
          )
        ) ORDER BY wmai.id, wmai.working_memory_id) as working_memory_answer_items
      FROM working_memory_answers wma
      LEFT JOIN working_memory_answer_items wmai ON wmai.working_memory_answer_id = wma.id
      LEFT JOIN assignments a ON a.id = wma.assignment_id
      LEFT JOIN assignment_items at ON at.assignment_id = a.id
      GROUP BY wma.id, a.id;
    }
    results = conn.execute(sql).to_a
    data = results[offset, limit]
    if data.blank?
      data = {
        id: [],
        assignment_id: [],
        assignment_item_id: [],
        answer_correct: [],
        answer_wrong: [],
        start_date: [],
        end_date: [],
        assignment_name: [],
        working_memory_answer_items: []
      }
    else
      data = data.map do |results|
        {
          id: results['id'],
          assignment_id: results['assignment_id'],
          assignment_item_id: results['assignment_item_id'],
          answer_correct: results['answer_correct'],
          answer_wrong: results['answer_wrong'],
          start_date: results['start_date'],
          end_date: results['end_date'],
          assignment_name: results['assignment_name'],
          working_memory_answer_items: JSON.parse(results['working_memory_answer_items']).map do |item|
            {
              'working_memory_answer_item_id': item['working_memory_answer_item_id'],
              'working_memory_id': item['working_memory_id'].to_i,
              'working_memory_answer_id': item['working_memory_answer_id'],
              'answers': item['answers'].map do |answer|
                {
                  'question': answer['question'].to_i,
                  'answer_element_id': answer['answer_element_id'].to_i,
                  'is_correct': (
                    WorkingMemoryItem.find_by(working_memory_id: answer['working_memory_id'].to_i, position: answer['question'].to_i)&.element&.id == answer['answer_element_id'].to_i ? true : false
                  ),
                  'working_memory_id': answer['working_memory_id'].to_i,
                  'answer_file_url': WorkingMemoryItem.find_by(working_memory_id: answer['working_memory_id'].to_i, position: answer['question'].to_i)&.element&.image_url
                }
              end
            }
          end
        }
      end
      
    end
    total_pages = (results.length.to_i / limit.to_i).ceil
    if params[:datatable] == 'true'
      return {
        data: data,
        total: results.length,
        total_page: total_pages,
        page: page,
        limit: limit
      }
    else
      data
    end
  end
  
end
