class Pipe < ApplicationRecord
  belongs_to :pipe_dimension
  belongs_to :element
  belongs_to :status
  has_many :pipe_items
  has_many :pipe_serie_stages

  attr_accessor :new_pipe_items
  
  after_save :save_pipe_items

  # validate :validate_pipe_items_count

  # def validate_pipe_items_count
  #   # Check if pipe_dimension_id is present and if it matches the expected row x col count
  #   if pipe_dimension_id.present?
  #     expected_count = pipe_dimension.row * pipe_dimension.col
  #     actual_count = new_pipe_items.count
  #     if expected_count != actual_count
  #       errors.add(:new_pipe_items, "must have exactly #{expected_count} items (#{pipe_dimension.row} x #{pipe_dimension.col})")
  #       return
  #     end
  #   end
  # end

  def as_json(options = {})
    super(except: [:pipe_dimension_id, :status_id, :created_at, :updated_at]).merge!(
      pipe_dimension: pipe_dimension,
      status: status.id,
      pipe_items: pipe_items
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:name, :code]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end

  def save_pipe_items
    p "--------------------#{new_pipe_items}-------------------"
    if new_pipe_items
      new_obj_ids = []
      new_pipe_items.each do |attr|
        obj = attr[:id].blank? ? pipe_items.build : PipeItem.find(attr[:id])
        obj.attributes = attr
        obj.save
        new_obj_ids << obj.id
      end
      #shop_group_shops :  call use relations shop_group_shops and shop_groups
      #find old Items and remove id not in new_obj_ids
      pipe_items.each { |e| e.destroy unless new_obj_ids.include?(e.id) }
      reload
    end
  end
  
end
