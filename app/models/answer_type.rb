class AnswerType < ApplicationRecord
  has_many :pipe_answer_items

  validates :name, presence: true , uniqueness: true

  def as_json(options = {})
    super().merge!(
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )
    # data = data.join("left join game_types on game_types.id = #{table_name}.game_type_id")
    # data = data.where(" #{table_name}.working_memory_serie_id = 1")
    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:name]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end
end
