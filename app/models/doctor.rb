class Doctor < ApplicationRecord
  has_many :assignments
  has_many :users
  belongs_to :role_permission
  belongs_to :gender

  attr_accessor :username, :password, :password_confirmation
  
  after_save :create_user

  def create_user
    # Create a new user associated with this doctor
    user = User.new(
      username: self.username, # Assuming username is the same as the doctor's username
      password: self.password, # Set the initial password
      password_confirmation: self.password_confirmation, # Confirm the password
      doctor_id: self.id, # Associate the user with this doctor
      role_permission_id: self.role_permission_id # Assign the role permission
    )

    if user.save
      # User was successfully created
      return true
    else
      # User creation failed
      errors.add(:user, 'User creation failed')
      return false
    end
  end

  def calulate_age(birthday)
    if birthday.present?
      now = Time.now.utc.to_date
      return now.year - birthday.year - (birthday.to_date.change(:year => now.year) > now ? 1 : 0)
    else
      return nil # Or handle this case as appropriate for your application
    end
  end

  def as_json(options = {})
    super(except:[:created_at, :updated_at]).merge(
      role_permission: role_permission&.name,
      full_name: Doctor.decode_name(self.first_name.to_s) + " " + Doctor.decode_name(self.last_name.to_s),
      first_name: Doctor.decode_name(self.first_name.to_s),
      last_name: Doctor.decode_name(self.last_name.to_s),
      gender_id: gender&.id,
      gender: gender&.name,
      Age: calulate_age(self.birthday),
      user_id: User.find_by(doctor_id: self.id)&.id,
    )
  end

  def self.search(params = {})
    data = all
    
    data = data.select %(
      #{table_name}.*,
      doctors.first_name || ' ' || doctors.last_name as full_name
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:"doctors.first_name || ' ' || doctors.last_name", :tel]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true

    if params[:order] == "full_name asc"
      params[:order] = Arel.sql("convert_from(decode(doctors.first_name, 'base64'), 'UTF-8'), convert_from(decode(doctors.last_name, 'base64'), 'UTF-8')")
    elsif params[:order] == "full_name desc"
      params[:order] = Arel.sql("convert_from(decode(doctors.first_name, 'base64'), 'UTF-8') desc, convert_from(decode(doctors.last_name, 'base64'), 'UTF-8') desc")
    end
    
    super(params.merge!(data: data))
  end

  def self.encode_name(str_name)
     Base64.strict_encode64(str_name).force_encoding("UTF-8")
  end

  # Decode the first name and last name
  def self.decode_name(str_name)
    begin
      decoded_name = Base64.strict_decode64(str_name).force_encoding('UTF-8')
    rescue ArgumentError
      # Handle the error, e.g., return an empty string or raise a custom exception
      decoded_name = '' # You can adjust this behavior as needed
    end
    decoded_name
  end

end
