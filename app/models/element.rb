class Element < ApplicationRecord
  include Rails.application.routes.url_helpers

  belongs_to :element_type
  belongs_to :game_type
  has_many :working_memory_items
  has_many :pipe_items

  attr_accessor :new_image

  after_commit :save_attachments, unless: :skip_callback?
  after_save :show_image
  
  has_one_attached :image
  def as_json(options = {})
    super(except: [:file_url, :element_type_id, :game_type_id, :created_at, :updated_at]).merge!(
      element_type: element_type&.name,
      element_type_id: element_type&.id,
      game_type: game_type&.name,
      game_type_id: game_type&.id,
      image_name: !self.image.attached? ? "" : self.image.filename.to_s,
      image_url: !self.image.attached? ? "" : rails_blob_path(self.image, only_path: true)
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = []
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end

  def save_attachments
    self.skip_callback = true
    
    # image
    if new_image
      image.purge
      image.attach new_image if new_image.present?
    end
  end

  def show_image
    if self.image.attached?
      image_url = rails_blob_path(self.image, only_path: true)
      self.update_column(:file_url, image_url)
    else
      p "Image is not attached."
    end
  end

  def image_url
    rails_blob_path(image, only_path: true) if image.attached?
  end
  
end
