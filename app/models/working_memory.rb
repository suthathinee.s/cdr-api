class WorkingMemory < ApplicationRecord
  belongs_to :working_memory_size
  belongs_to :status
  has_many :working_memory_items
  has_many :working_memory_serie_stages

  attr_accessor :new_working_memory_items
  
  after_save :save_working_memory_items

  # validate :validate_working_memory_items_count

  # def validate_working_memory_items_count
  #   # Check if working_memory_size_id is present and if it matches the number of new_working_memory_items
  #   if working_memory_size_id.present? && new_working_memory_items.count != working_memory_size.size
  #     errors.add(:new_working_memory_items, "must have exactly #{working_memory_size.size} items")
  #     return
  #   end
  # end

  def as_json(options = {})
    super(except: [:working_memory_size_id, :status_id, :created_at, :updated_at]).merge!(
      status: status&.name,
      working_memory_size: working_memory_size,
      working_memory_items: working_memory_items
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:name]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end

  def save_working_memory_items
    if new_working_memory_items
      new_obj_ids = []
      new_working_memory_items.each do |attr|
        obj = attr[:id].blank? ? working_memory_items.build : WorkingMemoryItem.find(attr[:id])
        obj.attributes = attr
        obj.save
        new_obj_ids << obj.id
      end
      #shop_group_shops :  call use relations shop_group_shops and shop_groups
      #find old Items and remove id not in new_obj_ids
      working_memory_items.each { |e| e.destroy unless new_obj_ids.include?(e.id) }
      reload
    end
  end
end
