class WorkingMemorySerieStage < ApplicationRecord
  belongs_to :working_memory_series
  belongs_to :working_memory
  
  def as_json(options = {})
    super(except: [:working_memory_series_id, :working_memory_id, :created_at, :updated_at]).merge!(
      working_memory: working_memory
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = []
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end
end
