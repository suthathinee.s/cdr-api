class AssignmentItem < ApplicationRecord
  belongs_to :assignment
  belongs_to :game_type
  belongs_to :working_memory_serie, foreign_key: :working_memory_serie_id, class_name: "WorkingMemorySeries"
  belongs_to :pipe_serie, foreign_key: :pipe_serie_id, class_name: "PipeSeries"
  has_many :assignment_answers

  def as_json(options = {})
    patient = Patient.find_by(id: assignment.patient_id)
    super(except: [:assignment_id, :game_type_id, :working_memory_serie_id, :pipe_serie_id, :created_at, :updated_at]).merge!(
      assignment_name: assignment&.name,
      game_type_id: game_type&.id,
      patient_full_name: Patient.decode_name(patient.first_name.to_s) + " " + Patient.decode_name(patient&.last_name),
      series_name: (game_type&.id == 1 ? working_memory_serie&.name : (game_type&.id == 2 ? pipe_serie&.name : nil)),
      working_memory_serie: working_memory_serie,
      pipe_serie: pipe_serie
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )
    # data = data.join("left join game_types on game_types.id = #{table_name}.game_type_id")
    # data = data.where(" #{table_name}.working_memory_serie_id = 1")
    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = []
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end
end
