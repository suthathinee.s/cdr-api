class Login < ApplicationRecord
  include TimeConversion
  #login
  SECRET_KEY = Rails.application.secrets.secret_key_base
  def self.create_token(params = {})
		# partner = KeyExternalConn.find_by(key: params[:key], secret: params[:secret])

		# if partner.present? 
			expire_date = 24.hours.from_now
			payload = {username: params, expire_date: expire_date}
			token = self.encode(payload)
			RedisController.set_redis(key: token, value: token)
			result = token
		# else
		# 	result = nil
		# end
	end

	def self.decode(token)
    begin
      decoded = JWT.decode(token, SECRET_KEY)[0]
      HashWithIndifferentAccess.new(decoded)
    rescue JWT::VerificationError => e
      return { error: "Signature verification failed: #{e.message}" }
      return nil
    rescue JWT::DecodeError => e
      return { error: "Token decoding error: #{e.message}" }
      return nil
    end
  end

	def self.encode(payload)
	access_token = JWT.encode(payload, SECRET_KEY)
	return access_token
	end

end
