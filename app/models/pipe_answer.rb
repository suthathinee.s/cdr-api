class PipeAnswer < ApplicationRecord
  belongs_to :assignment
  belongs_to :assignment_item
  has_many :pipe_answer_items

  attr_accessor :new_pipe_answer_items

  after_save :save_pipe_answer_items

  validate :validate_assignment_item

  def validate_assignment_item
    if assignment_item
      if assignment_item.game_type&.name != "Pipe" || assignment_item.assignment_id != assignment_id
        errors.add(:assignment_item, "is not valid for creating a Pipe Answer")
        return
      end
    end
  end

  def as_json(options = {})
    super().merge!(
      assignment_name: assignment&.name,
      patient_full_name: Patient.decode_name(assignment&.patient&.first_name.to_s) + " " + Patient.decode_name(assignment&.patient&.last_name.to_s),
      game_type_id: assignment_item&.game_type&.id,
      game_type: assignment_item&.game_type&.name,
      total_correct: pipe_answer_items.count { |item| item.answer_type_id == 1 },
      total_incorrect: pipe_answer_items.count { |item| item.answer_type_id == 2 },
      pipe_answer_items: pipe_answer_items.map do |pipe_answer_item_table|
        get_p = Pipe.find_by(id: pipe_answer_item_table.pipe_id)
          
        answers_array = []
    
        pipe_answer_item_table.answer_obj.each do |answer_obj|
          key_as_integer = answer_obj["pipe_id"].to_i
          file_url = Element.find_by(id: key_as_integer)&.image_url
    
          new_answer_obj = {
            "answer_element_id": key_as_integer,
            "position": answer_obj["position"],
            "rotation": answer_obj["rotation"],
            "answer_file_url": file_url
          }
    
          answers_array << new_answer_obj
        end
    
        {
          "id": pipe_answer_item_table.id,
          "pipe_answer_id": pipe_answer_item_table.pipe_answer_id,
          "answers": answers_array,
          "pipe_id": pipe_answer_item_table.pipe_id,
          "click_amount": pipe_answer_item_table.click_amount,
          "spent_time": pipe_answer_item_table.spent_time,
          "limit_time": pipe_answer_item_table.limit_time,
          "is_correct": (pipe_answer_item_table.answer_type_id == 1 ? true : (pipe_answer_item_table.answer_type_id == 2 ? false : nil))
        }
      end
    )
  end  

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:name]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end

  def save_pipe_answer_items
    p "--------------------#{new_pipe_answer_items}-------------------"
    p "--------------------#{self}-------------------"
    if new_pipe_answer_items
      item_amount = PipeAnswerItem.where(pipe_answer_id: self.id).count

        if item_amount < 6
          new_obj_ids = []
          new_pipe_answer_items.each do |attr|
            obj = attr[:id].blank? ? pipe_answer_items.build : WorkingMemorySerieStage.find(attr[:id])
            p "--------------------#{obj}-------------------"
            obj.attributes = attr
            obj.save
            new_obj_ids << obj.id
          end
        #shop_group_shops :  call use relations shop_group_shops and shop_groups
        #find old Items and remove id not in new_obj_ids
        # pipe_answer_items.each { |e| e.destroy unless new_obj_ids.include?(e.id) }
        reload
        else
          errors.add(:new_pipe_answer_items, "can have at most 6 items")
          return
        end
    end
  end
  
end