class GameType < ApplicationRecord
  has_many :elements
  has_many :test_set_items
  has_many :pipe_series
  has_many :working_memory_series
  
  def as_json(options = {})
    super().merge!(
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = [:name]
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = false
    
    super(params.merge!(data: data))
  end
end
