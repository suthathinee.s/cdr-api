class PipeSerieStage < ApplicationRecord
  belongs_to :pipe_serie
  belongs_to :pipe

  def as_json(options = {})
    super(except: [:pipe_series_id, :pipe_id, :created_at, :updated_at]).merge!(
      pipe: pipe
    )
  end

  def self.search(params = {})
    data = all

    data = data.select %(
      #{table_name}.*
    )

    params[:inner_joins] = []
    params[:left_joins] = []
    params[:keywords_columns] = []
    params[:order] = params[:order] || "#{table_name}.id"
    params[:use_as_json] = true
    
    super(params.merge!(data: data))
  end
end
