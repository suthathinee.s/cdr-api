namespace :migration do
  require "csv"
  require 'securerandom'

  task load_game_types: :environment do
    create_or_update_data class: GameType, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, name: "Working memory"},
        {id: 2, name: "Pipe"}
      ]
  end

  task load_element_types: :environment do
    create_or_update_data class: ElementType, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, name: "number"},
        {id: 2, name: "shape"},
        {id: 3, name: "pipe"},
        {id: 4, name: "faucet"},
        {id: 5, name: "water tank"},
      ]
  end

  task load_statuses: :environment do
    create_or_update_data class: Status, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, name: "active"},
        {id: 2, name: "inactive"}
      ]
  end

  task load_genders: :environment do
    create_or_update_data class: Gender, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, name: "ชาย"},
        {id: 2, name: "หญิง"},
        {id: 3, name: "ไม่ระบุ"}
      ]
  end

  task load_careers: :environment do
    create_or_update_data class: Career, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, name: "ข้าราชการ"},
        {id: 2, name: "พนักงานเอกชน"},
        {id: 3, name: "เจ้าของธุรกิจ"},
        {id: 4, name: "อาจารย์"},
        {id: 5, name: "นักศึกษา"},
        {id: 6, name: "นักเรียน"}
      ]
  end

  task load_role_permissions: :environment do
    create_or_update_data class: RolePermission, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, name: "admin"},
        {id: 2, name: "หมอ"},
        {id: 3, name: "นักกจิต"}
      ]
  end

  task load_working_memory_sizes: :environment do
    create_or_update_data class: WorkingMemorySize, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, name: "3", size: 3},
        {id: 2, name: "4", size: 4},
        {id: 3, name: "5", size: 5},
        {id: 4, name: "6", size: 6},
        {id: 5, name: "7", size: 7},
        {id: 6, name: "8", size: 8},
        {id: 7, name: "9", size: 9}
      ]
  end

  task load_pipe_dimension: :environment do
    create_or_update_data class: PipeDimension, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, row: 3, col: 3, name: "3x3"},
        {id: 2, row: 4, col: 4, name: "4x4"},
        {id: 3, row: 5, col: 5, name: "5x5"},
        {id: 4, row: 6, col: 6, name: "6x6"}
      ]
  end

  task load_answer_type: :environment do
    create_or_update_data class: AnswerType, keys: [:id], reset_pkey: false,
      data: [
        {id: 1, name: "Pass"},
        {id: 2, name: "Failed"},
      ]
  end

  task load_elements: :environment do
    elements_data = [
      # working_memory_images - number
      # ----------------------------------------------------------------------------------------------------------------------------
      {
        id: 1,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/one.svg'
      },
      {
        id: 2,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/two.svg'
      },
      {
        id: 3,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/three.svg'
      },
      {
        id: 4,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/four.svg'
      },
      {
        id: 5,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/five.svg'
      },
      {
        id: 6,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/six.svg'
      },
      {
        id: 7,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/seven.svg'
      },
      {
        id: 8,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/eight.svg'
      },
      {
        id: 9,
        game_type_id: 1,
        element_type_id: 1,
        new_image: 'app/assets/images/working_memory_images/number/nine.svg'
      },
      # working_memory_images - shape
      # ----------------------------------------------------------------------------------------------------------------------------
      {
        id: 10,
        game_type_id: 1,
        element_type_id: 2,
        new_image: 'app/assets/images/working_memory_images/shape/triangle.svg'
      },
      {
        id: 11,
        game_type_id: 1,
        element_type_id: 2,
        new_image: 'app/assets/images/working_memory_images/shape/square.svg'
      },
      {
        id: 12,
        game_type_id: 1,
        element_type_id: 2,
        new_image: 'app/assets/images/working_memory_images/shape/pentagon.svg'
      },
      {
        id: 13,
        game_type_id: 1,
        element_type_id: 2,
        new_image: 'app/assets/images/working_memory_images/shape/octagon.svg'
      },
      {
        id: 14,
        game_type_id: 1,
        element_type_id: 2,
        new_image: 'app/assets/images/working_memory_images/shape/hexagon.svg'
      },
      # Pipe game - curve
      # ----------------------------------------------------------------------------------------------------------------------------
      {
        id: 15,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve.svg'
      },
      {
        id: 16,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_A.svg'
      },
      {
        id: 17,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_B.svg'
      },
      {
        id: 18,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_B_F.svg'
      },
      {
        id: 19,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_B_F_C.svg'
      },
      {
        id: 20,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_F.svg'
      },
      {
        id: 21,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_F_C.svg'
      },
      {
        id: 22,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_R.svg'
      },
      {
        id: 23,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_R_F.svg'
      },
      {
        id: 24,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/curve/curve_R_F_C.svg'
      },
      # Pipe game - line
      # ----------------------------------------------------------------------------------------------------------------------------
      {
        id: 25,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line.svg'
      },
      {
        id: 26,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_A.svg'
      },
      {
        id: 27,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_A_C.svg'
      },
      {
        id: 28,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_F.svg'
      },
      {
        id: 29,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_L.svg'
      },
      {
        id: 30,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_L_F.svg'
      },
      {
        id: 31,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_L_F_C.svg'
      },
      {
        id: 32,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_R.svg'
      },
      {
        id: 33,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_R_F.svg'
      },
      {
        id: 34,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/line/line_R_F_C.svg'
      },
      # Pipe game - three way
      # ----------------------------------------------------------------------------------------------------------------------------
      {
        id: 35,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways.svg'
      },
      {
        id: 36,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_A.svg'
      },
      {
        id: 37,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_B.svg'
      },
      {
        id: 38,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_BL.svg'
      },
      {
        id: 39,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_BR.svg'
      },
      {
        id: 40,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_F.svg'
      },
      {
        id: 41,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_F_C.svg'
      },
      {
        id: 42,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_L.svg'
      },
      {
        id: 44,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_LR.svg'
      },
      {
        id: 45,
        game_type_id: 2,
        element_type_id: 3,
        new_image: 'app/assets/images/pipe_images/three_ways/three_ways_R.svg'
      },
      # Pipe game - start/faucet
      # ----------------------------------------------------------------------------------------------------------------------------
      {
        id: 46,
        game_type_id: 2,
        element_type_id: 4,
        new_image: 'app/assets/images/pipe_images/start/start.svg'
      },
      {
        id: 47,
        game_type_id: 2,
        element_type_id: 4,
        new_image: 'app/assets/images/pipe_images/start/start_F.svg'
      },
      {
        id: 48,
        game_type_id: 2,
        element_type_id: 4,
        new_image: 'app/assets/images/pipe_images/start/start_F_C.svg'
      },
      # Pipe game - stop/water tank
      # ----------------------------------------------------------------------------------------------------------------------------
      {
        id: 49,
        game_type_id: 2,
        element_type_id: 5,
        new_image: 'app/assets/images/pipe_images/stop/stop.svg'
      },
      {
        id: 50,
        game_type_id: 2,
        element_type_id: 5,
        new_image: 'app/assets/images/pipe_images/stop/stop_F.svg'
      },
      {
        id: 51,
        game_type_id: 2,
        element_type_id: 5,
        new_image: 'app/assets/images/pipe_images/stop/stop_F_C.svg'
      }
    ]
  
    elements_data.each do |element_data|
      element = Element.find_or_initialize_by(id: element_data[:id])
      element.attributes = element_data.except(:new_image)
  
      if element.save
        # Check if the image file exists and is readable
        if File.exist?(element_data[:new_image]) && File.readable?(element_data[:new_image])
          # Open and attach the image file to the 'image' attachment
          element.image.attach(io: File.open(element_data[:new_image]), filename: File.basename(element_data[:new_image]))
        else
          puts "Image file not found or not readable: #{element_data[:new_image]}"
        end
  
        puts "Element with ID #{element.id} created or updated."
      else
        puts "Failed to create or update Element with ID #{element.id}. Errors: #{element.errors.full_messages.join(', ')}"
      end
    end
  end
    

  # task load_users: :environment do
  #   create_or_update_data class: User, keys: [:id], reset_pkey: false,
  #     data: [
  #       {id: 1,username: "sysadmin", password: "sysadmin", role_permission_id: 1}
  #     ]
  # end

  task load_users: :environment do
    create_or_update_data class: User, keys: [:id], reset_pkey: false,
      data: [
        {
          id: 1,
          username: "sysadmin",
          password: "sysadmin",
          password_confirmation: "sysadmin",  # Add password_confirmation
          role_permission_id: 1
        }
      ]
  end

  private

  def create_or_update_data(hsh = {})
    cls = hsh[:class]
    arr_data = hsh[:data]
    keys = hsh[:keys]

    if hsh[:reset_pkey]
      cls.delete_all
      ActiveRecord::Base.connection.reset_pk_sequence!(cls.table_name)
    end

    new_record_count = 0
    arr_data.each do |data|
      keys_hsh = {}
      keys.each do |k|
        keys_hsh[k] = (data.class == String ? data : data[k])
      end

      # fix => {id:1, name:""} will not increment pkey sequence
      data.delete(:id) if data.class != String && data[:id].present?
      
      obj = cls.where(keys_hsh).first || cls.new
      new_record_count += 1 if obj.new_record?
      obj.attributes = (data.class == String ? keys_hsh : data)
      obj.save
    end
    p "#{new_record_count} #{cls} created"
  end
end
